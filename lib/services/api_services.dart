import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:esquelas_app/model/user_model.dart';
import 'package:esquelas_app/services/service_global.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

ValueNotifier<UserModel> currentUser = new ValueNotifier(UserModel());

class ApiServices {
  ServiceGlobal objServices = ServiceGlobal();

  Future<int> login(
      String email,
      String pass,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/usuarios/login";
    Map map = {
      "email": email,
      "pass": pass,
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200 && (json.decode(response)['tipos_id'].toString() == "1")) {
      currentUser.value = UserModel.fromJSON(json.decode(response));
      print(currentUser.value);
      return 1;
    } else if (objServices.ok == 200 && (json.decode(response)['tipos_id'].toString() == "2") && (json.decode(response)['estado'].toString() == "1")) {
      currentUser.value = UserModel.fromJSON(json.decode(response));
      print(currentUser.value);
      return 2;
    } else {
      return 0;
    }
  }

  Future<int> updateUser(
      String id,
      String nombre,
      String apellido,
      String correo,
      String tel,
      String direccion,
      String img
      ) async {
    String url = "https://funerarias.herokuapp.com/api/usuarios/update_usuario_"+id;
    Map map = {
      "nombre": nombre,
      "apellido": apellido,
      "email": correo,
      "tel": tel,
      "direccion": direccion,
      "img": img
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> updateEsquela(
      String id,
      String nombre,
      String apellido,
      String titulo,
      String descripcion,
      String funeraria_empresas_id,
      String cementerio_empresa_id
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas/update_"+id;
    Map map = {
      "nombre": nombre,
      "apellido": apellido,
      "titulo": titulo,
      "descripcion": descripcion,
      "funeraria_empresas_id": funeraria_empresas_id,
      "cementerio_empresa_id": cementerio_empresa_id
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> getLoadDataUser(
      String id,
      ) async {
    String response = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/usuarios/get_'+id));
    print(response);
    if (objServices.ok == 200) {
      currentUser.value = UserModel.fromJSON(json.decode(response));
      print(currentUser.value);
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> registerUser(
      String nombre,
      String apellido,
      String email,
      String pass,
      String tel,
      int tipo_id,
      String img,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/usuarios/add";
    Map map = {
      "nombre": nombre,
      "apellido": apellido,
      "email": email,
      "pass" : pass,
      "tel" : tel,
      "direccion" : "",
      "img" : img,
      "tipos_id" : tipo_id,
      "empresas_cantidad" : 0,
      "esquelas_cantidad" : 0,
      "pesame_cantidad" : 0,
      "estado" : 1
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      currentUser.value = UserModel.fromJSON(json.decode(response)["datos"]);
      print(currentUser.value);
      return 1;
    } else {
      return 0;
    }
  }
  Future<int> registerAdmin(
      String nombre,
      String apellido,
      String email,
      String pass,
      String tel,
      int tipo_id,
      String img,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/usuarios/add";
    Map map = {
      "nombre": nombre,
      "apellido": apellido,
      "email": email,
      "pass" : pass,
      "tel" : tel,
      "direccion" : "",
      "img" : img,
      "tipos_id" : tipo_id,
      "empresas_cantidad" : 0,
      "esquelas_cantidad" : 0,
      "pesame_cantidad" : 0,
      "estado" : 1
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<void> logout() async {
    currentUser.value = new UserModel();
  }

  Future<List> getListUser() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/usuarios/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListEsquela() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/esquelas/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListEsquelaId(
      String id
      ) async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/esquelas/get_'+id));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      print(jsonResponse);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListEsquelabuy() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/esquelas_precios/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      print(jsonResponse);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<String> addEsquela(
      String nombre,
      String apellido,
      String titulo,
      String descripcion,
      String usuarios_id,
      String funeraria_empresas_id,
      String cementerio_empresa_id,
      String fecha_ini,
      String hora_ini,
      String fecha_ini_entierro,
      String hora_ini_entierro,
      String img,
      String id_img
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas/add";
    Map map = {
      "titulo": titulo,
      "descripcion": descripcion,
      "img" : img,
      "fondo_img_id" : id_img,
      "funeraria_empresas_id" : funeraria_empresas_id,
      "funeraria_direccion" : "",
      "cementerio_empresa_id" : cementerio_empresa_id,
      "cementerio_direccion" : "" ,
      "fecha_ini" : fecha_ini,
      "fecha_fin" : "2000-01-01",
      "hora_ini" : hora_ini,
      "hora_fin" : "00:00:00",
      "estado" : "1",
      "esquelas_textos_id" : "1",
      "esquelas_tipos_id" : "1",
      "usuarios_id" : usuarios_id,
      "nombre" : nombre,
      "apellido" : apellido,
      "fecha_ini_entierro" : fecha_ini_entierro,
      "hora_ini_entierro" : hora_ini_entierro,
      "hora_fin_entierro" : "00:00:00"
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(response);
      print(jsonResponse["datos"]["id"]);
      return jsonResponse["datos"]["id"].toString();
    } else {
      return "";
    }
  }

  Future<int> addEsquelaBuy(
      String usuarios_id,
      String precios_id,
      String precio,
      String esquela_id,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas_ventas_usuarios/add";
    Map map = {
      "usuarios_id": usuarios_id,
      "precios_id": precios_id,
      "precio" : precio,
      "cantidad" : "1",
      "esquela_id" : esquela_id,
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<List> getListImgEsquela() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/esquelas_img/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<int> deleteEsquela(
      String id
      ) async {
    String res = (await objServices.apiRequestDelete(
        'https://funerarias.herokuapp.com/api/esquelas/delete_'+id));
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<List> getListCategory() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/empresas_productos_categorias/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListCompany(
      String id,
      ) async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/empresas/categoria_'+id));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListCompanyTotal(
      String tipos,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/empresas/tipos";
    Map map = {
      "tipos": tipos,
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(response);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListProduct(
      String id
      ) async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/empresas_productos/empresa_'+id));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListProductid() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/empresas_productos/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> BuscarEsquela (
      String busqueda,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas/search";
    Map map = {
      "busqueda": busqueda,
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(response);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> BuscarEsquelaDate (
      String fecha_ini,
      String fecha_fin
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas/rango_fechas";
    Map map = {
      "fecha_ini": fecha_ini,
      "fecha_fin": fecha_fin
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(response);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<int> updateStateUser(
      String id,
      String estado
      ) async {
    String status = "";
    if (estado == "1") {
      status = "Desactivaste";
      estado = "2";
    } else {
      status = "Activaste";
      estado = "1";
    }
    String url = "https://funerarias.herokuapp.com/api/usuarios/update_"+id;
    Map map = {
      "estado": estado
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> updateStateCompany(
      String id,
      String estado
      ) async {
    String status = "";
    if (estado == "1") {
      status = "Desactivaste";
      estado = "2";
    } else {
      status = "Activaste";
      estado = "1";
    }
    String url = "https://funerarias.herokuapp.com/api/empresas/update_"+id;
    Map map = {
      "estado": estado
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> updateStateProduct(
      String id,
      String estado
      ) async {
    String status = "";
    if (estado == "1") {
      status = "Desactivaste";
      estado = "2";
    } else {
      status = "Activaste";
      estado = "1";
    }
    String url = "https://funerarias.herokuapp.com/api/empresas_productos/update_"+id;
    Map map = {
      "estado": estado
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> updateStateEsquela(
      String id,
      String estado
      ) async {
    String status = "";
    if ((estado == "1") || (estado == "2") || (estado == "3")) {
      status = "Desactivaste";
      estado = "0";
    } else {
      status = "Activaste";
      estado = "1";
    }
    String url = "https://funerarias.herokuapp.com/api/esquelas/update_"+id;
    Map map = {
      "estado": estado
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> updateStateFinalizeEsquela(
      String id,
      String estado
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas/update_"+id;
    Map map = {
      "estado": estado
    };
    final response = await objServices.apiRequestPut(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<String> updatePhotoUser(
      File image
      ) async {
    final mimeTypeData = lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    final imageUploadRequest = http.MultipartRequest(
        'POST',
        Uri.parse("https://api.cloudinary.com/v1_1/dksstmpfu/image/upload?upload_preset=fxfc1clo"));
    final file = await http.MultipartFile.fromPath('file', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    //final folder = await http.MultipartFile.fromPath('folder', 'test/esquela');
    imageUploadRequest.files.add(file);
    final streamedResponse = await imageUploadRequest.send();
    final response = await http.Response.fromStream(streamedResponse);
    print("IMAGENNNNNN");
    print(response.body);
    if (response.statusCode == 200) {
      final jsonResponse = json.decode(response.body);
      return jsonResponse['url'];
    } else {
      return "";
    }
  }

  Future<int> addEmpresaProductoVenta(
      String empresas_productos_id,
      String usuarios_id,
      String direccion_entrega,
      String texto,
      String hora,
      String fecha,
      String cantidad,
      String deposito,
      String imagen_deposito,
      String esquela_id,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/empresas_productos_ventas/add";
    Map map = {
      "empresas_productos_id": empresas_productos_id,
      "usuarios_id": usuarios_id,
      "direccion_entrega" : direccion_entrega,
      "texto" : texto,
      "hora" : hora,
      "fecha" : fecha,
      "cantidad" : cantidad,
      "deposito" : deposito,
      "imagen_deposito" : imagen_deposito,
      "esquela_id" : esquela_id,
      "estado" : "1",
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<List> getListProductBuy() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/empresas_productos_ventas/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      print(jsonResponse);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<List> getListChat() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/esquelas_mensajes_luto/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      print(jsonResponse);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<int> addPrayersEsquela(
      String mensaje,
      String esquela_id,
      String estado,
      String fechahora,
      String categoria_mensaje,
      String usuario_id,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas_mensajes_luto/add";
    Map map = {
      "mensaje": mensaje,
      "esquela_id": esquela_id,
      "estado" : estado,
      "fechahora" : fechahora,
      "categoria_mensaje" : categoria_mensaje,
      "usuario_id" : usuario_id,
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<int> addImgEsquela(
      String titulo,
      String link,
      ) async {
    String url = "https://funerarias.herokuapp.com/api/esquelas_img/add";
    Map map = {
      "titulo": titulo,
      "link": link,
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }

  Future<List> getListTipoEmpresa() async {
    String res = (await objServices.apiRequestGet(
        'https://funerarias.herokuapp.com/api/empresas_tipos/get_all'));
    if (objServices.ok == 200) {
      final jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      return [];
    }
  }

  Future<int> registerCompany(
      String titulo,
      String descripcion,
      String direccion,
      String telefono,
      String email,
      String pass,
      String logo,
      String usuarios_id,
      String tipos_id,
      String categorias_id
      ) async {
    String url = "https://funerarias.herokuapp.com/api/empresas/add";
    Map map = {
      "titulo": titulo,
      "descripcion": descripcion,
      "direccion": direccion,
      "telefono" : telefono,
      "email" : email,
      "pass" : pass,
      "logo" : logo,
      "usuarios_id" : usuarios_id,
      "tipos_id" : tipos_id,
      "categorias_id" : categorias_id,
      "estado" : 1,
      "long" : "",
      "lat" : ""
    };
    final response = await objServices.apiRequestPost(url, map);
    print(response);
    if (objServices.ok == 200) {
      return 1;
    } else {
      return 0;
    }
  }
}
