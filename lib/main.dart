import 'package:esquelas_app/ui/login.dart';
import 'package:esquelas_app/util/colors.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreenn(),
    );
  }
}

class SplashScreenn extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreennState();
  }
}

class SplashScreennState extends State<SplashScreenn> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
        seconds: 5,
        navigateAfterSeconds: LoginWidget(),
        image: Image.asset(
          'assets/img/img_icon.png',
          height: 150,
          width: 150,
        ),
        backgroundColor: Colors.transparent,
        photoSize: 100.0,
        loaderColor: Colors.black45
    );
  }
}