import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/alert_add_prayers.dart';
import 'package:esquelas_app/ui/user/page/esquela.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/material.dart';

class PrayersEsquela extends StatefulWidget {
  var img;
  String esquela_id,
      nombre,
      apellido,
      titulo,
      descripcion,
      fecha_ini,
      fecha_fin,
      hora_ini,
      hora_fin,
      fecha_ini_entierro,
      hora_ini_entierro,
      hora_fin_entierro,
      funeraria_empresas_id,
      cementerio_empresa_id,
      page
  ;
  PrayersEsquela({Key key,
    this.img,
    this.esquela_id,
    this.nombre,
    this.apellido,
    this.titulo,
    this.descripcion,
    this.fecha_ini,
    this.fecha_fin,
    this.hora_ini,
    this.hora_fin,
    this.fecha_ini_entierro,
    this.hora_ini_entierro,
    this.hora_fin_entierro,
    this.cementerio_empresa_id,
    this.funeraria_empresas_id,
    this.page
  }) : super(key: key);
  @override
  _PrayersEsquelaState createState() => _PrayersEsquelaState();
}

class _PrayersEsquelaState extends State<PrayersEsquela> {
  ApiServices objApiServices = ApiServices();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return EsquelaWidget(img: widget.img, esquela_id: widget.esquela_id, nombre: widget.nombre, apellido: widget.apellido, titulo: widget.titulo, descripcion: widget.descripcion, fecha_ini: widget.fecha_ini, fecha_fin: widget.fecha_fin, hora_ini: widget.hora_ini, hora_fin: widget.hora_fin, fecha_ini_entierro: widget.fecha_ini_entierro, hora_ini_entierro: widget.hora_ini_entierro, hora_fin_entierro: widget.hora_fin_entierro, cementerio_empresa_id: widget.cementerio_empresa_id, funeraria_empresas_id: widget.funeraria_empresas_id, page: widget.page);
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              return Navigator.of(context).pushReplacement(PageRouteBuilder(
                  pageBuilder: (BuildContext context, _, __) {
                    return EsquelaWidget(img: widget.img, esquela_id: widget.esquela_id, nombre: widget.nombre, apellido: widget.apellido, titulo: widget.titulo, descripcion: widget.descripcion, fecha_ini: widget.fecha_ini, fecha_fin: widget.fecha_fin, hora_ini: widget.hora_ini, hora_fin: widget.hora_fin, fecha_ini_entierro: widget.fecha_ini_entierro, hora_ini_entierro: widget.hora_ini_entierro, hora_fin_entierro: widget.hora_fin_entierro, cementerio_empresa_id: widget.cementerio_empresa_id, funeraria_empresas_id: widget.funeraria_empresas_id, page: widget.page);
                  }, transitionsBuilder:
                  (_, Animation<double> animation, __, Widget child) {
                return FadeTransition(opacity: animation, child: child);
              }));
            },
            icon: Icon(
              Icons.arrow_back,
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.near_me),
              onPressed: () async {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertAddPrayers(img: widget.img, esquela_id: widget.esquela_id, nombre: widget.nombre, apellido: widget.apellido, titulo: widget.titulo, descripcion: widget.descripcion, fecha_ini: widget.fecha_ini, fecha_fin: widget.fecha_fin, hora_ini: widget.hora_ini, hora_fin: widget.hora_fin, fecha_ini_entierro: widget.fecha_ini_entierro, hora_ini_entierro: widget.hora_ini_entierro, hora_fin_entierro: widget.hora_fin_entierro, cementerio_empresa_id: widget.cementerio_empresa_id, funeraria_empresas_id: widget.funeraria_empresas_id, page: widget.page);
                    },useRootNavigator: false,
                    barrierDismissible: false
                );
              },
            )
          ],
          centerTitle: true,
          title: Text("Condolencias"),
        ),
        body: chat(),
      ),
    );
  }

  Widget chat(){
    Responsive responsive = Responsive.of(context);
    return Container(
      height: responsive.heigth,
      child: FutureBuilder(
        future: objApiServices.getListChat(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if (!snapshot.hasData) {
            return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ));
          }else if (snapshot.data.length == 0) {
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 14,
                            ),
                            Text("No hay oraciones")
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            );
          }else{
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index){
                  String esquela_id = snapshot.data[index]["esquela_id"].toString();
                  String usuario_id = snapshot.data[index]["usuario_id"].toString();
                  String mensaje = snapshot.data[index]["mensaje"].toString();
                  String fechahora = snapshot.data[index]["fechahora"].toString();
                  String fecha = fechahora.split('T')[0];
                  String horaReducir = fechahora.split('T')[1];
                  String hora = horaReducir.split('.')[0];
                  if(esquela_id == widget.esquela_id){
                    return Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        children: <Widget>[
                          usuario_id==currentUser.value.id.toString()
                              ?Row(
                            mainAxisAlignment:
                            MainAxisAlignment.end,
                            children: [
                              Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.end,
                                children: [
                                  Container(
                                      margin: const EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.end,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                                children: [
                                                  Container(
                                                    constraints: BoxConstraints(
                                                        maxWidth: MediaQuery.of(
                                                            context)
                                                            .size
                                                            .width *
                                                            0.6),
                                                    padding:
                                                    const EdgeInsets.all(
                                                        15.0),
                                                    decoration: BoxDecoration(
                                                      color: Colors.black54,
                                                      borderRadius:
                                                      BorderRadius.only(
                                                        topRight:
                                                        Radius.circular(
                                                            25),
                                                        topLeft:
                                                        Radius.circular(
                                                            25),
                                                        bottomLeft:
                                                        Radius.circular(
                                                            25),
                                                      ),
                                                    ),
                                                    child: Text(mensaje, style: TextStyle(color: Colors.white),),
                                                  ),
                                                  SizedBox(height: 3,),
                                                  Text(hora+" "+fecha),
                                                ],
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                  ),
                                ],
                              ),
                            ],
                          )
                          :Row(
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            children: [
                              Column(
                                children: <Widget>[
                                  Container(
                                    width: responsive.wp(10),
                                    height: responsive.wp(10),
                                    child: FutureBuilder(
                                      future: objApiServices.getListUser(),
                                      builder: (BuildContext context, AsyncSnapshot snapshot){
                                        if (!snapshot.hasData) {
                                          return Container(
                                              child: Center(
                                                child: CircularProgressIndicator(),
                                              ));
                                        }else if (snapshot.data.length == 0) {
                                          return Container(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Column(
                                                        children: <Widget>[
                                                          SizedBox(
                                                            height: 14,
                                                          ),
                                                          Text("No hay oraciones")
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          );
                                        }else{
                                          return ListView.builder(
                                            itemCount: snapshot.data.length,
                                            itemBuilder: (context, index){
                                              String id = snapshot.data[index]["id"].toString();
                                              String img = snapshot.data[index]["img"].toString();
                                              if(id == usuario_id){
                                                return Container(
                                                  width: responsive.wp(10),
                                                  height: responsive.wp(10),
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                      color: Colors.white,
                                                      width: 3,
                                                    ),
                                                    boxShadow: [
                                                      BoxShadow(
                                                          color: Colors.grey,
                                                          offset: Offset(0, 2),
                                                          blurRadius: 5)
                                                    ],
                                                  ),
                                                  child: CircleAvatar(
                                                    backgroundImage: NetworkImage(img),
                                                  ),
                                                );
                                              }else{
                                                return Container();
                                              }
                                            }
                                          );
                                        }
                                      },
                                    )
                                  ),
                                  SizedBox(height: 5),
                                  Container(
                                    width: responsive.wp(10),
                                    height: responsive.hp(2),
                                      child: FutureBuilder(
                                        future: objApiServices.getListUser(),
                                        builder: (BuildContext context, AsyncSnapshot snapshot){
                                          if (!snapshot.hasData) {
                                            return Container(
                                                child: Center(
                                                  child: CircularProgressIndicator(),
                                                ));
                                          }else if (snapshot.data.length == 0) {
                                            return Container(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: <Widget>[
                                                      Flexible(
                                                        child: Column(
                                                          children: <Widget>[
                                                            SizedBox(
                                                              height: 14,
                                                            ),
                                                            Text("No hay oraciones")
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            );
                                          }else{
                                            return ListView.builder(
                                                itemCount: snapshot.data.length,
                                                itemBuilder: (context, index){
                                                  String id = snapshot.data[index]["id"].toString();
                                                  String nombre = snapshot.data[index]["nombre"].toString();
                                                  if(id == usuario_id){
                                                    return Text(nombre, style: TextStyle(color: Colors.grey, fontSize: responsive.dp(1.6)), maxLines: 1, textAlign: TextAlign.center,);
                                                  }else{
                                                    return Container();
                                                  }
                                                }
                                            );
                                          }
                                        },
                                      )
                                  ),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      margin: const EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    constraints: BoxConstraints(
                                                        maxWidth: MediaQuery.of(
                                                            context)
                                                            .size
                                                            .width *
                                                            .6),
                                                    padding:
                                                    const EdgeInsets.all(
                                                        15.0),
                                                    decoration: BoxDecoration(
                                                      color: Colors.black,
                                                      borderRadius:
                                                      BorderRadius.only(
                                                        topLeft:
                                                        Radius.circular(
                                                            25),
                                                        topRight:
                                                        Radius.circular(
                                                            25),
                                                        bottomRight:
                                                        Radius.circular(
                                                            25),
                                                      ),
                                                    ),
                                                    child: Text(
                                                      mensaje,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .body1
                                                          .apply(
                                                        color:
                                                        Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(height: 3,),
                                                  Text(hora+" "+fecha),
                                                ],
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  }else{
                    return Container();
                  }
                }
            );
          }
        }
      ),
    );
  }
}
