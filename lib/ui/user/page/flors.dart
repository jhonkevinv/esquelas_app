import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/list_esquela.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/colors.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FlorsWidget extends StatefulWidget {
  String producto_id, page, titulo, img, precio, descripcion, esquela_id;
  FlorsWidget({Key key, this.producto_id, this.page, this.titulo, this.img, this.precio, this.descripcion, this.esquela_id}) : super(key: key);
  @override
  _FlorsWidgetState createState() => new _FlorsWidgetState();
}

class _FlorsWidgetState extends State<FlorsWidget> {

  ApiServices objApiServices = ApiServices();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  TextEditingController notaText = TextEditingController();
  TextEditingController direccionText = TextEditingController();
  TextEditingController nombreEntregaText = TextEditingController();

  String cantidad;
  List<String> cantidades = <String>[
    '1',
    '2',
    '3',
    '4',
    '5',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Text("Comprar producto"),
      ),
      body: body(context),
    );
  }

  Widget body(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        Container(
          height: responsive.hp(31),
          width: responsive.width,
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(90), bottomRight: Radius.circular(90)),
              image: DecorationImage(
                  image: NetworkImage(widget.img),
                  fit: BoxFit.fill
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 30, left: 30, bottom: 20),
          child: Column(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 35, left: 35),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Text(widget.titulo, textAlign: TextAlign.start, style: TextStyle(fontSize: responsive.dp(1.7), fontWeight: FontWeight.bold),),
                              ),
                              Expanded(
                                flex: 1,
                                child: Text("Q"+widget.precio, textAlign: TextAlign.end, style: TextStyle(fontSize: responsive.dp(1.7), fontWeight: FontWeight.bold),),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: responsive.hp(12),
                            width: responsive.wp(80),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black, //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: ListView(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(widget.descripcion, textAlign: TextAlign.center,),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Text("Cantidad:", textAlign: TextAlign.start, style: TextStyle(fontSize: responsive.dp(1.7), fontWeight: FontWeight.bold),),
                              ),
                              Expanded(
                                flex: 1,
                                child: DropdownButtonFormField<String>(
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                      borderSide: BorderSide(
                                        color: Colors.black,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                      borderSide: BorderSide(
                                        color: Colors.black,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                      borderSide: BorderSide(
                                        color: Colors.black,
                                      ),
                                    ),
                                    hintText: "0",
                                    hintStyle: TextStyle(color: Colors.black, fontSize: responsive.dp(1.7)),
                                  ),
                                  items: cantidades.map<DropdownMenuItem<String>>((item) {
                                    return DropdownMenuItem<String>(
                                      child: Text(
                                        item,
                                        style: TextStyle(color: Colors.black, fontSize: responsive.dp(1.7)),
                                      ),
                                      value: item,
                                    );
                                  }).toList(),
                                  onChanged: (value) async {
                                    setState(() {
                                      cantidad = value;
                                    });
                                  },
                                  validator: (input) => input == null ? "Seleccionar Cantidad" : null,
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: TextFormField(
                                  controller: notaText,
                                  keyboardType: TextInputType.text,
                                  validator: (input) =>
                                  input.length == 0 ? "Ingresar Nota" : null,
                                  maxLines: 3,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(10),
                                    hintText: "Nota",
                                    hintStyle: TextStyle(color: Colors.black),
                                    labelStyle: TextStyle(color: Colors.black),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: primaryColorFont,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.black,
                                        width: 1.0,
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: primaryColorFont,
                                        width: 1.0,
                                      ),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.black,
                                        width: 1.0,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: direccionText,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Dirección" : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Dirección",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: nombreEntregaText,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar nombre de entrega" : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Nombre de entrega",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: responsive.hp(3)),
              ButtonTheme(
                height: responsive.hp(6.3),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () {
                    if (_formKey.currentState
                        .validate()) {
                      if(widget.page == "1"){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ListEsquela(pageEsquela: "2", producto_id: widget.producto_id, direccionText: direccionText.text, notaText: notaText.text, cantidad: cantidad),));
                      }else{
                        addEsquela();
                        //_scaffoldKey.currentState.showSnackBar(SnackBar(
                        //  content: Text('Compras en mantenimiento'),
                        //  duration: Duration(seconds: 3),
                        //  backgroundColor: Color(0XFF707070),
                        //));
                      }
                    }
                  },
                  textColor: Colors.white,
                  color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: Text(
                      "COMPRAR",
                      style: TextStyle(fontSize: responsive.dp(1.8)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  addEsquela() async {
    String formattedHora = DateFormat('kk:mm:ss').format(DateTime.now());
    String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
    int status = 0;
    status = await objApiServices.addEmpresaProductoVenta(
        widget.producto_id,
        currentUser.value.id.toString(),
        direccionText.text,
        notaText.text,
        formattedHora,
        formattedDate,
        cantidad,
        "",
        "",
        widget.esquela_id,
    );
    if (status == 1) {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return messageAlert(tipo: "compra",);
          },useRootNavigator: false,
          barrierDismissible: false
      );
      //_scaffoldKey.currentState.showSnackBar(SnackBar(
      //  content: Text('Compra registrada'),
      //  duration: Duration(seconds: 3),
      //));
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Compra no registrado'),
        duration: Duration(seconds: 3),
      ));
    }
  }
}