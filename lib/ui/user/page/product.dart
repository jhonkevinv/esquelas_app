import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/flors.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProductPage extends StatefulWidget {

  String page, id, esquela_id;
  ProductPage({Key key, this.page, this.id, this.esquela_id}) : super(key: key);
  @override
  _ProductPageState createState() => new _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  ApiServices objApiService = ApiServices();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.black,
          automaticallyImplyLeading: true,
          centerTitle: true,
          title: Text("PRODUCTOS"),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Container(
            height: responsive.heigth,
            child: FutureBuilder(
              future: objApiService.getListProduct(widget.id),
              builder: (BuildContext context, AsyncSnapshot snapshot){
                if (!snapshot.hasData) {
                  return Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ));
                }else if (snapshot.data.length == 0) {
                  return Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 14,
                                  ),
                                  Text("No hay datos")
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  );
                }else{
                  return ListView.builder (
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index){
                      String id = snapshot.data[index]["id"].toString();
                      String titulo = snapshot.data[index]["titulo"].toString();
                      String img = snapshot.data[index]["img"].toString();
                      String precio = snapshot.data[index]["precio"].toString();
                      String estado = snapshot.data[index]["estado"].toString();
                      String descripcion = snapshot.data[index]["descripcion"].toString();
                      if(estado == "1"){
                        return Container (
                            child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => FlorsWidget(page: widget.page, producto_id: id, esquela_id: widget.esquela_id, titulo: titulo, img: img, precio: precio, descripcion: descripcion),));
                              },
                              child: Card(
                                elevation: 1,
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Colors.white70, width: 1),
                                  borderRadius: BorderRadius.circular(40),
                                ),
                                margin: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              width: responsive.wp(28),
                                              height: responsive.wp(28),
                                              decoration: BoxDecoration(
                                                  borderRadius: new BorderRadius.circular(30.0),
                                                  border: Border.all(
                                                    color: Colors.black,
                                                    width: 1.0,
                                                  ),
                                                  image: DecorationImage(
                                                      image: CachedNetworkImageProvider(img),
                                                      fit: BoxFit.fill
                                                  )
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          children: <Widget>[
                                            Text(titulo, textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(2), fontWeight: FontWeight.bold),),
                                            Text("Q"+precio, textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(1.6)),),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ),
                            )
                        );
                      }else{
                        return Container();
                      }
                    },
                  );
                }
              },
            ),
          )
        )
    );
  }
}