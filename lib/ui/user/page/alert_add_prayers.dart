import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/prayers_esquela.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/colors.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:lottie/lottie.dart';
import 'package:intl/intl.dart';

class AlertAddPrayers extends StatefulWidget {
  var img;
  String esquela_id,
      nombre,
      apellido,
      titulo,
      descripcion,
      fecha_ini,
      fecha_fin,
      hora_ini,
      hora_fin,
      fecha_ini_entierro,
      hora_ini_entierro,
      hora_fin_entierro,
      funeraria_empresas_id,
      cementerio_empresa_id,
      page;
  AlertAddPrayers({Key key,
    this.img,
    this.esquela_id,
    this.nombre,
    this.apellido,
    this.titulo,
    this.descripcion,
    this.fecha_ini,
    this.fecha_fin,
    this.hora_ini,
    this.hora_fin,
    this.fecha_ini_entierro,
    this.hora_ini_entierro,
    this.hora_fin_entierro,
    this.cementerio_empresa_id,
    this.funeraria_empresas_id,
    this.page}) : super(key: key);
  @override
  _AlertAddPrayersState createState() => _AlertAddPrayersState();
}

class _AlertAddPrayersState extends State<AlertAddPrayers> {
  TextEditingController notaText = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  ApiServices objApiServices = ApiServices();
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive.of(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        title: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: responsive.wp(6),
                    height: responsive.wp(6),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.red
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("x", style: TextStyle(color: Colors.white, fontSize: responsive.dp(1.8)),)
                      ],
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 10,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("Enviar condolencia", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
              ],
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  child: Lottie.asset('assets/lottie/send_lottie.json',
                      width: responsive.wp(50), height: responsive.hp(18)),
                ),
                Form(
                  key: _formKey,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: responsive.wp(50),
                        child: TextFormField(
                          controller: notaText,
                          keyboardType: TextInputType.text,
                          validator: (input) =>
                          input.length == 0 ? "Ingresar Condolencia" : null,
                          maxLines: 2,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(10),
                            hintText: "Condolencia",
                            hintStyle: TextStyle(color: Colors.black),
                            labelStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: primaryColorFont,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: primaryColorFont,
                                width: 1.0,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 10,),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: responsive.wp(20),
                              height: 50,
                              buttonColor: Colors.black,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0)),
                                onPressed: (){
                                  if (_formKey.currentState
                                      .validate()){
                                    addEsquela();
                                  }
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text("Aceptar",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  addEsquela() async {
    String fechaHora = DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now());
    int status = 0;
    status = await objApiServices.addPrayersEsquela(
        notaText.text,
        widget.esquela_id,
        "1",
        fechaHora,
        "1",
        currentUser.value.id.toString(),
    );
    if (status == 1) {
      Navigator.of(context).pushReplacement(PageRouteBuilder(
          pageBuilder: (BuildContext context, _, __) {
            return PrayersEsquela(img: widget.img, esquela_id: widget.esquela_id, nombre: widget.nombre, apellido: widget.apellido, titulo: widget.titulo, descripcion: widget.descripcion, fecha_ini: widget.fecha_ini, fecha_fin: widget.fecha_fin, hora_ini: widget.hora_ini, hora_fin: widget.hora_fin, fecha_ini_entierro: widget.fecha_ini_entierro, hora_ini_entierro: widget.hora_ini_entierro, hora_fin_entierro: widget.hora_fin_entierro, cementerio_empresa_id: widget.cementerio_empresa_id, funeraria_empresas_id: widget.funeraria_empresas_id, page: widget.page);
          }, transitionsBuilder:
          (_, Animation<double> animation, __, Widget child) {
        return FadeTransition(opacity: animation, child: child);
      }));
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return messageAlert(tipo: "condolencia",);
        },useRootNavigator: false,
        barrierDismissible: false,
      );
    } else {
      print("Oracion no agregada");
    }
  }
}
