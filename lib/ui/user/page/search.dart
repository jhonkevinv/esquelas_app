import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/esquela.dart';
import 'package:esquelas_app/ui/user/tab/page_user.dart';
import 'package:esquelas_app/util/colors.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class SearchWidget extends StatefulWidget {

  @override
  _SearchWidgetState createState() => new _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  ApiServices objApiService = ApiServices();
  String name;
  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return WillPopScope(
      onWillPop: (){
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return PageUserWidget(page: 0,);
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            automaticallyImplyLeading: false,
            leading: IconButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(PageRouteBuilder(
                    pageBuilder: (BuildContext context, _, __) {
                      return PageUserWidget(page: 0,);
                    }, transitionsBuilder:
                    (_, Animation<double> animation, __, Widget child) {
                  return FadeTransition(opacity: animation, child: child);
                }));
              },
              icon: Icon(
                Icons.arrow_back,
              ),
            ),
            centerTitle: true,
            title: Text("BUSCAR"),
          ),
          body: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        onSubmitted: (text){
                          setState(() {
                            name=text;
                          });
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(0),
                          prefixIcon: Icon(Icons.search, color: Colors.black,),
                          hintText: "Buscar",
                          hintStyle: TextStyle(color: Colors.black),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: primaryColorFont,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.black,
                              width: 1.0,
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.black,
                              width: 1.0,
                            ),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.black,
                              width: 1.0,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: responsive.hp(80),
                child: FutureBuilder(
                  future: objApiService.BuscarEsquela(name),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ));
                    }else if (snapshot.data.length == 0) {
                      return Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 14,
                                      ),
                                      Text("No hay esquela")
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      );
                    }else{
                      return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          var img = snapshot.data[index]["img"];
                          String esquela_id = snapshot.data[index]["id"].toString();
                          String nombre = snapshot.data[index]["nombre"];
                          String apellido = snapshot.data[index]["apellido"];
                          String titulo = snapshot.data[index]["titulo"];
                          String descripcion = snapshot.data[index]["descripcion"];
                          String fecha_ini = snapshot.data[index]["fecha_ini"];
                          String fecha_fin = snapshot.data[index]["fecha_fin"];
                          String hora_ini = snapshot.data[index]["hora_ini"];
                          String hora_fin = snapshot.data[index]["hora_fin"];
                          String fecha_ini_entierro = snapshot.data[index]["fecha_ini_entierro"];
                          String hora_ini_entierro = snapshot.data[index]["hora_ini_entierro"];
                          String hora_fin_entierro = snapshot.data[index]["hora_fin_entierro"];
                          String funeraria_empresas_id = snapshot.data[index]["funeraria_empresas_id"].toString();
                          String cementerio_empresa_id = snapshot.data[index]["cementerio_empresa_id"].toString();
                          String estado = snapshot.data[index]["estado"].toString();
                          String fondo_img_id = snapshot.data[index]["fondo_img_id"].toString();
                          if((estado != "0") && (estado != "1") && (estado != "3")){
                            return GestureDetector(
                              onTap: (){
                                Navigator.of(context).pushReplacement(PageRouteBuilder(
                                    pageBuilder: (BuildContext context, _, __) {
                                      return EsquelaWidget(img: img, esquela_id: esquela_id, nombre: nombre, apellido: apellido, titulo: titulo, descripcion: descripcion, fecha_ini: fecha_ini, fecha_fin: fecha_fin, hora_ini: hora_ini, hora_fin: hora_fin, fecha_ini_entierro: fecha_ini_entierro, hora_ini_entierro: hora_ini_entierro, hora_fin_entierro: hora_fin_entierro, cementerio_empresa_id: cementerio_empresa_id, funeraria_empresas_id: funeraria_empresas_id, page: "search");
                                    }, transitionsBuilder:
                                    (_, Animation<double> animation, __, Widget child) {
                                  return FadeTransition(opacity: animation, child: child);
                                }));
                              },
                              child: Container(
                                height: responsive.hp(30),
                                child: FutureBuilder(
                                  future: objApiService.getListImgEsquela(),
                                  builder: (context, snapshot) {
                                    if (!snapshot.hasData) {
                                      return Container(
                                          child: Center(
                                            child: CircularProgressIndicator(),
                                          ));
                                    }else if (snapshot.data.length == 0) {
                                      return Container(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Flexible(
                                                  child: Column(
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 14,
                                                      ),
                                                      Text("No hay datos")
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      );
                                    }else{
                                      return ListView.builder(
                                          shrinkWrap: true,
                                          physics: const NeverScrollableScrollPhysics(),
                                          itemCount: snapshot.data.length,
                                          itemBuilder: (context, index){
                                            String link = snapshot.data[index]["link"].toString();
                                            String id = snapshot.data[index]["id"].toString();
                                            if(fondo_img_id == id){
                                              return Card(
                                                  shape: RoundedRectangleBorder(
                                                    side: BorderSide(color: Colors.white70, width: 1),
                                                    borderRadius: BorderRadius.circular(40),
                                                  ),
                                                  margin: EdgeInsets.all(10.0),
                                                  elevation: 0,
                                                  child: Container(
                                                    height: responsive.hp(28),
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(40),
                                                      border: Border.all(
                                                        color: Colors
                                                            .black, //                   <--- border color
                                                        width: 1.5,
                                                      ),
                                                      image: DecorationImage(
                                                        image: CachedNetworkImageProvider(link),
                                                        fit: BoxFit.fill,
                                                        colorFilter: ColorFilter.mode(
                                                            Colors.grey.withOpacity(1),
                                                            BlendMode.softLight
                                                        ),
                                                      ),
                                                    ),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          flex: 1,
                                                          child: Column(
                                                            children: <Widget>[
                                                              Text("")
                                                            ],
                                                          ),
                                                        ),
                                                        Expanded(
                                                            flex: 1,
                                                            child: Padding(
                                                              padding: const EdgeInsets.only(top: 20, bottom: 20, right: 30),
                                                              child: Column(
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Container(
                                                                    width: responsive.wp(25),
                                                                    height: responsive.wp(25),
                                                                    decoration: BoxDecoration(
                                                                        shape: BoxShape.circle,
                                                                        image: DecorationImage(
                                                                            image: CachedNetworkImageProvider(img),
                                                                            fit: BoxFit.fill
                                                                        )
                                                                    ),
                                                                  ),
                                                                  SizedBox(height: 10,),
                                                                  Text(nombre, textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(2.2), color: Colors.black, fontWeight: FontWeight.bold), maxLines: 1,),
                                                                  Text(apellido, textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(2.2), color: Colors.black, fontWeight: FontWeight.bold), maxLines: 1,),
                                                                ],
                                                              ),
                                                            )
                                                        )
                                                      ],
                                                    ),
                                                  )
                                              );
                                            }else{
                                              return Container();
                                            }
                                          }
                                      );
                                    }
                                  },
                                ),
                              )
                            );
                          }else{
                            return Container();
                          }
                        },
                      );
                    }
                  },
                ),
              )
            ],
          )
      ),
    );
  }
}