import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/list_esquela.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditEsquelaWidget extends StatefulWidget {

  String id, img, nombre, apellido, titulo, descripcion, funeraria_empresas_id, cementerio_empresa_id, estado;
  EditEsquelaWidget({Key key, this.id, this.nombre, this.img, this.apellido, this.titulo, this.descripcion, this.funeraria_empresas_id, this.cementerio_empresa_id, this.estado}) : super(key: key);

  @override
  _EditEsquelaWidgetState createState() => new _EditEsquelaWidgetState();
}

class _EditEsquelaWidgetState extends State<EditEsquelaWidget> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  ApiServices objApiServices = ApiServices();
  TextEditingController nombreText = TextEditingController();
  TextEditingController apellidoText = TextEditingController();
  TextEditingController tituloText = TextEditingController();
  TextEditingController descripcionText = TextEditingController();

  String funeraria;
  List<String> funerarias = <String>[
    'Funeraria 1',
    'Funeraria 2'
  ];

  String cementerio;
  List<String> cementerios = <String>[
    'cementerio 1',
    'cementerio 2'
  ];

  @override
  void initState() {
    super.initState();
    this.nombreText.text = widget.nombre;
    this.apellidoText.text = widget.apellido;
    this.tituloText.text = widget.titulo;
    this.descripcionText.text = widget.descripcion;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return ListEsquela(pageEsquela: "1");
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Text("EDITAR ESQUELA"),
          leading: IconButton(
            onPressed: () {
              return Navigator.of(context).pushReplacement(PageRouteBuilder(
                  pageBuilder: (BuildContext context, _, __) {
                    return ListEsquela(pageEsquela: "1");
                  }, transitionsBuilder:
                  (_, Animation<double> animation, __, Widget child) {
                return FadeTransition(opacity: animation, child: child);
              }));
            },
            icon: Icon(
              Icons.arrow_back,
            ),
          ),
          actions: <Widget>[
            widget.estado=="2"
            ?IconButton(
              icon: Icon(
                Icons.cancel,
                color: Colors.white,
              ),
              onPressed: () {
                return showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return messagePermition(idEsquela: widget.id,tipo: "finalizar",);
                    },useRootNavigator: false,
                    barrierDismissible: false
                );
              },
            )
            :Container(),
            IconButton(
              icon: Icon(
                Icons.delete,
                color: Colors.white,
              ),
              onPressed: () {
                return showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return messagePermition(idEsquela: widget.id,tipo: "delete",);
                    },useRootNavigator: false,
                    barrierDismissible: false
                );
              },
            )
          ],
        ),
        body: editProfile(context),
      ),
    );
  }

  Widget editProfile(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        Container(
          height: responsive.hp(31),
          width: responsive.width,
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(90), bottomRight: Radius.circular(90)),
              border: Border.all(
                color: Colors
                    .black, //                   <--- border color
                width: 1.5,
              ),
              image: DecorationImage(
                  image: NetworkImage(widget.img),
                  fit: BoxFit.cover
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 30, left: 30, bottom: 20),
          child: Column(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Nombre" : null,
                            controller: nombreText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Nombre",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Apellido" : null,
                            controller: apellidoText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Apellido",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Titulo" : null,
                            controller: tituloText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Titulo",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Descripción" : null,
                            controller: descripcionText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Descripción",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          //width: responsive.width,
                          child: FutureBuilder(
                              future:
                              objApiServices.getListCompanyTotal("2"),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      child: Center(
                                        child:
                                        CircularProgressIndicator(),
                                      ));
                                }
                                if (snapshot.data.length == 0) {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Funeraria",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        funeraria = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Funeraria" : null,
                                  );
                                } else {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Funeraria",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    value: widget.funeraria_empresas_id,
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        funeraria = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Funeraria" : null,
                                  );
                                }
                              }),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          //width: responsive.width,
                          child: FutureBuilder(
                              future:
                              objApiServices.getListCompanyTotal("3"),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      child: Center(
                                        child:
                                        CircularProgressIndicator(),
                                      ));
                                }
                                if (snapshot.data.length == 0) {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Cementerio",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        cementerio = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Cementerio" : null,
                                  );
                                } else {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Cementerio",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    value: widget.cementerio_empresa_id,
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        cementerio = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Cementerio" : null,
                                  );
                                }
                              }),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: responsive.hp(3)),
              ButtonTheme(
                height: responsive.hp(6.3),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () {
                    if (_formKey.currentState
                        .validate()) {
                      updateEsquela();
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text('Cargando ...'),
                        duration: Duration(seconds: 3),
                        backgroundColor: Color(0XFF707070),
                      ));
                    }
                  },
                  textColor: Colors.white,
                  color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: Text(
                      "Cambiar",
                      style: TextStyle(fontSize: responsive.dp(1.8)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  updateEsquela() async {
    int status = 0;
    status = await objApiServices.updateEsquela(
      widget.id,
      nombreText.text,
      apellidoText.text,
      tituloText.text,
      descripcionText.text,
      funeraria,
      cementerio
    );
    if (status == 1) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Esquela Modificada'),
        duration: Duration(seconds: 3),
      ));
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('La esquela no pudo ser Modificada'),
        duration: Duration(seconds: 3),
      ));
    }
  }

}