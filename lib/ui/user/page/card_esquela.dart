import 'dart:io';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class CardEsquela extends StatefulWidget {
  File foto;
  String id_img, init_foto, tipo, nombre, apellido, titulo, descripcion, funeraria, cementerio, fechaFuneraria, horaVelorio, fechaCementerio, horaEntierro;
  CardEsquela({Key key, this.id_img, this.foto, this.init_foto, this.nombre, this.tipo , this.apellido, this.titulo, this.descripcion, this.funeraria, this.cementerio, this.fechaFuneraria, this.horaVelorio, this.fechaCementerio, this.horaEntierro}) : super(key: key);
  @override
  _CardEsquelaState createState() => _CardEsquelaState();
}

class _CardEsquelaState extends State<CardEsquela> {
  ApiServices objApiServices = ApiServices();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String price;
  String price_id;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Text("Pagar esquela"),
      ),
      body: card(),
    );
  }
  Widget card(){
    Responsive responsive = Responsive.of(context);
    return Center(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Lottie.asset('assets/lottie/card_lottie.json',
                        width: responsive.wp(90), height: responsive.hp(25)),
                  ),
                  SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text("Precio:", textAlign: TextAlign.end,),
                      ),
                      SizedBox(width: 10,),
                      Expanded(
                        flex: 1,
                        child: Container(
                          height: responsive.hp(3),
                          child: FutureBuilder(
                            future: objApiServices.getListEsquelabuy(),
                            builder: (context, snapshot) {
                              if (!snapshot.hasData) {
                                return Container(
                                    child: Center(
                                      child: CircularProgressIndicator(),
                                    ));
                              }else if (snapshot.data.length == 0) {
                                return Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Flexible(
                                            child: Column(
                                              children: <Widget>[
                                                Text("No hay datos")
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                              }else{
                                return Center(
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (context, index){
                                        String id = snapshot.data[index]["id"].toString();
                                        String precio = snapshot.data[index]["precio"].toString();
                                        if(id == "1"){
                                          price = precio;
                                          price_id = id;
                                          return Text("Q"+precio);
                                        }else{
                                          return Container();
                                        }
                                      }
                                  ),
                                );
                              }
                            },
                          ),
                        )
                      )
                    ],
                  ),
                  SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Nombre del titular:", textAlign: TextAlign.start,),
                      )
                    ],
                  ),
                  SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          validator: (input) =>
                          input.length == 0 ? "Ingresar Nombre" : null,
                          //controller: nombreText,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(left: 20, right: 20),
                            hintText: "",
                            hintStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Numero de tarjeta:", textAlign: TextAlign.start,),
                      )
                    ],
                  ),
                  SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (input) =>
                          input.length < 16 ? "Ingresar numero" : null,
                          //controller: nombreText,
                          textAlign: TextAlign.center,
                          maxLength: 16,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(left: 20, right: 20),
                            hintText: "",
                            hintStyle: TextStyle(color: Colors.black),
                            counterStyle: TextStyle(height: double.minPositive,),
                            counterText: "",
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text("Fecha de vencimiento:", textAlign: TextAlign.start,),
                                )
                              ],
                            ),
                            SizedBox(height: 5,),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: responsive.wp(15),
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    validator: (input) =>
                                    input.length < 2 ? "Datos no validos" : null,
                                    //controller: nombreText,
                                    textAlign: TextAlign.center,
                                    maxLength: 2,
                                    maxLines: 1,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(left: 10, right: 10),
                                      hintText: "mes",
                                      hintStyle: TextStyle(color: Colors.black),
                                      counterStyle: TextStyle(height: double.minPositive,),
                                      counterText: "",
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 5,),
                                Text("/",style: TextStyle(fontSize: 25),),
                                SizedBox(width: 5,),
                                Container(
                                  width: responsive.wp(20),
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    validator: (input) =>
                                    input.length < 4 ? "Datos no validos" : null,
                                    //controller: nombreText,
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    maxLength: 4,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(left: 10, right: 10),
                                      hintText: "año",
                                      hintStyle: TextStyle(color: Colors.black),
                                      counterStyle: TextStyle(height: double.minPositive,),
                                      counterText: "",
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text("Codigo cvv:", textAlign: TextAlign.start,),
                                )
                              ],
                            ),
                            SizedBox(height: 5,),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: responsive.wp(20),
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    validator: (input) =>
                                    input.length < 3 ? "cvv no validos" : null,
                                    //controller: nombreText,
                                    textAlign: TextAlign.center,
                                    maxLength: 3,
                                    maxLines: 1,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(left: 20, right: 20),
                                      hintText: "cvv",
                                      hintStyle: TextStyle(color: Colors.black),
                                      counterStyle: TextStyle(height: double.minPositive,),
                                      counterText: "",
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Correo electronico:", textAlign: TextAlign.start,),
                      )
                    ],
                  ),
                  SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          validator: (input) =>
                          input.length == 0 || !input.contains("@") ? "Ingresar correo" : null,
                          //controller: nombreText,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(left: 20, right: 20),
                            hintText: "",
                            hintStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20,),
                  ButtonTheme(
                    height: responsive.hp(6.3),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      onPressed: () {
                        if (_formKey.currentState
                            .validate()){
                          if(widget.tipo == "sin foto"){
                            _updateImage();
                          }else if(widget.tipo == "foto"){
                            addEsquela(widget.init_foto);
                          }
                        }
                      },
                      textColor: Colors.white,
                      color: Colors.black,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 20, left: 20),
                        child: Text(
                          "Pagar",
                          style: TextStyle(fontSize: responsive.dp(1.8)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _updateImage() async{
    String estado = "";
    estado = await objApiServices.updatePhotoUser(widget.foto);
    if(estado==""){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Imagen no cargada'),
        duration: Duration(seconds: 3),
      ));
    }else{
      addEsquela(estado);
    }
  }

  addEsquela(String estado) async {
    String status = "";
    status = await objApiServices.addEsquela(
        widget.nombre,
        widget.apellido,
        widget.titulo,
        widget.descripcion,
        currentUser.value.id.toString(),
        widget.funeraria,
        widget.cementerio,
        widget.fechaFuneraria,
        widget.horaVelorio,
        widget.fechaCementerio,
        widget.horaEntierro,
        estado,
        widget.id_img
    );
    if (status == "") {
      print("Esquela no registrado");
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Esquela no registrado'),
        duration: Duration(seconds: 3),
      ));
    } else {
      addEsquelaVentas(status);
    }
  }

  addEsquelaVentas(String estado) async {
    int status = 0;
    status = await objApiServices.addEsquelaBuy(
        currentUser.value.id.toString(),
        price_id,
        price,
        estado,
    );
    if (status == 1) {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return messageAlert(tipo: "esquelaAgregada",);
        },useRootNavigator: false,
        barrierDismissible: false,
      );
    } else {
      print("Venta de esquela no registrado");
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Esquela no registrado'),
        duration: Duration(seconds: 3),
      ));
    }
  }
}
