import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PurchaseHistoryWidget extends StatefulWidget {
  @override
  _PurchaseHistoryWidgetState createState() => _PurchaseHistoryWidgetState();
}

class _PurchaseHistoryWidgetState extends State<PurchaseHistoryWidget> {

  ApiServices objApiService = ApiServices();

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("COMPRAS"),
        backgroundColor: Colors.black,
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Container(
        height: responsive.heigth,
        child: FutureBuilder(
          future: objApiService.getListProductBuy(),
          builder: (BuildContext context, AsyncSnapshot snapshot){
            if (!snapshot.hasData) {
              return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ));
            }else if (snapshot.data.length == 0) {
              return Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 14,
                              ),
                              Text("No hay datos")
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            }else{
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index){
                  String usuarios_id = snapshot.data[index]["usuarios_id"].toString();
                  String esquela_id = snapshot.data[index]["esquela_id"].toString();
                  String cantidad = snapshot.data[index]["cantidad"].toString();
                  String empresas_productos_id = snapshot.data[index]["empresas_productos_id"].toString();
                  if((usuarios_id == currentUser.value.id.toString()) && (esquela_id != "0")){
                    return Padding(
                      padding: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Container(
                                      width: responsive.wp(35),
                                      height: responsive.wp(35),
                                      child: FutureBuilder(
                                          future: objApiService.getListProductid(),
                                          builder: (BuildContext context, AsyncSnapshot snapshot){
                                            if (!snapshot.hasData) {
                                              return Container(
                                                  child: Center(
                                                    child: CircularProgressIndicator(),
                                                  ));
                                            }else if (snapshot.data.length == 0) {
                                              return Container(
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: <Widget>[
                                                        Flexible(
                                                          child: Column(
                                                            children: <Widget>[
                                                              SizedBox(
                                                                height: 14,
                                                              ),
                                                              Text("No hay datos")
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }else{
                                              return ListView.builder(
                                                  shrinkWrap: true,
                                                  physics: const NeverScrollableScrollPhysics(),
                                                  itemCount: snapshot.data.length,
                                                  itemBuilder: (context, index){
                                                    String id = snapshot.data[index]["id"].toString();
                                                    String img = snapshot.data[index]["img"].toString();
                                                    String precio = snapshot.data[index]["precio"].toString();
                                                    if(id == empresas_productos_id){
                                                      return Container(
                                                        width: responsive.wp(35),
                                                        height: responsive.wp(35),
                                                        decoration: BoxDecoration(
                                                            borderRadius: new BorderRadius.circular(30.0),
                                                            border: Border.all(
                                                              color: Colors.black,
                                                              width: 1.0,
                                                            ),
                                                            image: DecorationImage(
                                                                image: NetworkImage(img),
                                                                fit: BoxFit.fill
                                                            )
                                                        ),
                                                      );
                                                    }else{
                                                      return Container();
                                                    }
                                                  }
                                              );
                                            }
                                          }
                                      ),
                                    ),
                                  )
                                ],
                              )
                          ),
                          Expanded(
                              flex: 1,
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("Precio:", style: TextStyle(fontSize: responsive.dp(1.7), fontWeight: FontWeight.bold),),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: responsive.hp(2),
                                            child: FutureBuilder(
                                                future: objApiService.getListProductid(),
                                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                                  if (!snapshot.hasData) {
                                                    return Container(
                                                        child: Center(
                                                          child: CircularProgressIndicator(),
                                                        ));
                                                  }else if (snapshot.data.length == 0) {
                                                    return Container(
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: <Widget>[
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                            children: <Widget>[
                                                              Flexible(
                                                                child: Column(
                                                                  children: <Widget>[
                                                                    SizedBox(
                                                                      height: 14,
                                                                    ),
                                                                    Text("No hay datos")
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  }else{
                                                    return ListView.builder(
                                                        shrinkWrap: true,
                                                        physics: const NeverScrollableScrollPhysics(),
                                                        itemCount: snapshot.data.length,
                                                        itemBuilder: (context, index){
                                                          String precio = snapshot.data[index]["precio"].toString();
                                                          String id = snapshot.data[index]["id"].toString();
                                                          if(id == empresas_productos_id){
                                                            return Padding(
                                                              padding: const EdgeInsets.only(left: 10, right: 10),
                                                              child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Text(precio, style: TextStyle(fontSize: responsive.dp(1.5),),),
                                                                ],
                                                              ),
                                                            );
                                                          }else{
                                                            return Container();
                                                          }
                                                        }
                                                    );
                                                  }
                                                }
                                            ),
                                          )
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("Cantidad:", style: TextStyle(fontSize: responsive.dp(1.7), fontWeight: FontWeight.bold),),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.only(left: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text(cantidad, style: TextStyle(fontSize: responsive.dp(1.5),),),
                                              ],
                                            ),
                                          )
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("Para:", style: TextStyle(fontSize: responsive.dp(1.7), fontWeight: FontWeight.bold),),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: responsive.hp(6),
                                            child: FutureBuilder(
                                                future: objApiService.getListEsquela(),
                                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                                  if (!snapshot.hasData) {
                                                    return Container(
                                                        child: Center(
                                                          child: CircularProgressIndicator(),
                                                        ));
                                                  }else if (snapshot.data.length == 0) {
                                                    return Container(
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: <Widget>[
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                            children: <Widget>[
                                                              Expanded(
                                                                child: Text("No hay datos")
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  }else{
                                                    return Center(
                                                      child: ListView.builder(
                                                          shrinkWrap: true,
                                                          physics: const NeverScrollableScrollPhysics(),
                                                          itemCount: snapshot.data.length,
                                                          itemBuilder: (context, index){
                                                            String id = snapshot.data[index]["id"].toString();
                                                            String nombre = snapshot.data[index]["nombre"].toString();
                                                            String apellido = snapshot.data[index]["apellido"].toString();
                                                            if(id == esquela_id){
                                                              return Padding(
                                                                padding: const EdgeInsets.only(left: 10),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: <Widget>[
                                                                    Text(nombre+" "+apellido, style: TextStyle(fontSize: responsive.dp(1.5),),),
                                                                  ],
                                                                ),
                                                              );
                                                            }else{
                                                              return Container();
                                                            }
                                                          }
                                                      ),
                                                    );
                                                  }
                                                }
                                            ),
                                          )
                                      )
                                    ],
                                  )
                                ],
                              )
                          )
                        ],
                      ),
                    );
                  }else{
                    return Container();
                  }
                }
              );
            }
          }
        ),
      ),
    );
  }
}
