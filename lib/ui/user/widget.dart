import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/login.dart';
import 'package:esquelas_app/ui/user/page/list_esquela.dart';
import 'package:esquelas_app/ui/user/tab/page_user.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class messageAlert extends StatefulWidget {
  String tipo;
  messageAlert({Key key, this.tipo}) : super(key: key);
  @override
  _messageAlertState createState() => _messageAlertState();
}

class _messageAlertState extends State<messageAlert> {
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive.of(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        title: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                if(widget.tipo=="compra")
                Text("Producto Comprado", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else if(widget.tipo=="esquelaAgregada")
                Text("Esquela agregada", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else if(widget.tipo=="esquelaEliminada")
                Text("Esquela eliminada con exito", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else if(widget.tipo=="esquelaFinalizada")
                Text("Esquela finalizada con exito", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else if(widget.tipo=="condolencia")
                Text("Condolencia enviada con exito", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else if(widget.tipo=="fondo")
                Text("Fondo agregado con exito", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else if(widget.tipo=="empresa")
                Text("Empresa agregada con exito", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
              ],
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  child: Lottie.asset('assets/lottie/check_lottie.json',
                      width: responsive.wp(90), height: responsive.hp(25)),
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: ButtonTheme(
                        minWidth: 300,
                        height: 50,
                        buttonColor: Colors.black,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0)),
                          onPressed: (){
                            if(widget.tipo=="compra"){
                              Navigator.of(context).pushReplacement(PageRouteBuilder(
                                  pageBuilder: (BuildContext context, _, __) {
                                    return PageUserWidget(page: 0,);
                                  }, transitionsBuilder:
                                  (_, Animation<double> animation, __, Widget child) {
                                return FadeTransition(opacity: animation, child: child);
                              }));
                            }else if(widget.tipo=="esquelaAgregada"){
                              Navigator.of(context).pushReplacement(PageRouteBuilder(
                                  pageBuilder: (BuildContext context, _, __) {
                                    return PageUserWidget(page: 1,);
                                  }, transitionsBuilder:
                                  (_, Animation<double> animation, __, Widget child) {
                                return FadeTransition(opacity: animation, child: child);
                              }));
                            }else{
                              Navigator.pop(context);
                            }
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class messagePermition extends StatefulWidget {
  String idEsquela, tipo;
  messagePermition({Key key, this.idEsquela, this.tipo}) : super(key: key);
  @override
  _messagePermitionState createState() => _messagePermitionState();
}

class _messagePermitionState extends State<messagePermition> {
  ApiServices objApiServices = ApiServices();
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive.of(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        title: Column(
          children: <Widget>[
            if(widget.tipo=="delete")
            Text("¿Desea eliminar la esquela?", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)),textAlign: TextAlign.center,)
            else if(widget.tipo=="session")
            Text("¿Desea cerrar sesión?", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)),textAlign: TextAlign.center,)
            else if(widget.tipo=="finalizar")
            Text("¿Desea finalizar la esquela?", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)),textAlign: TextAlign.center,),
          ],
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                if(widget.tipo=="delete")
                Container(
                  child: Lottie.asset('assets/lottie/delete_lottie.json',
                      width: responsive.wp(90), height: responsive.hp(25)),
                )
                else if(widget.tipo=="session")
                Container(
                  child: Lottie.asset('assets/lottie/logout_lottie.json',
                      width: responsive.wp(90), height: responsive.hp(25)),
                )
                else if(widget.tipo=="finalizar")
                Container(
                  child: Lottie.asset('assets/lottie/question_lottie.json',
                      width: responsive.wp(90), height: responsive.hp(25)),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          ButtonTheme(
                            minWidth: responsive.wp(20),
                            height: 50,
                            buttonColor: Colors.black,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25.0)),
                              onPressed: (){
                                Navigator.pop(context);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text("No",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ),
                    SizedBox(width: 20,),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          ButtonTheme(
                            minWidth: responsive.wp(20),
                            height: 50,
                            buttonColor: Colors.black,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25.0)),
                              onPressed: () async {
                                if(widget.tipo=="delete"){
                                  deleteEsquela();
                                }else if(widget.tipo=="session"){
                                  await objApiServices.logout().then((value) {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => LoginWidget(),));
                                  });
                                }else if(widget.tipo=="finalizar"){
                                  updateStateFinalizeEsquela();
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text("Si",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  deleteEsquela() async {
    int status = 0;
    status = await objApiServices.deleteEsquela(
      widget.idEsquela,
    );
    if (status == 1) {
      print("Esquela eliminada");
      Navigator.of(context).pushReplacement(PageRouteBuilder(
          pageBuilder: (BuildContext context, _, __) {
            return ListEsquela(pageEsquela: "1");
          }, transitionsBuilder:
          (_, Animation<double> animation, __, Widget child) {
        return FadeTransition(opacity: animation, child: child);
      }));
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return messageAlert(tipo: "esquelaEliminada",);
        },useRootNavigator: false,
        barrierDismissible: false,
      );
    } else {
      print("La esquela no pudo ser eliminado");
    }
  }
  updateStateFinalizeEsquela() async {
    int status = 0;
    status = await objApiServices.updateStateFinalizeEsquela(
      widget.idEsquela,
      "3"
    );
    if (status == 1) {
      print("Esquela Finalizada");
      Navigator.of(context).pushReplacement(PageRouteBuilder(
          pageBuilder: (BuildContext context, _, __) {
            return ListEsquela(pageEsquela: "1");
          }, transitionsBuilder:
          (_, Animation<double> animation, __, Widget child) {
        return FadeTransition(opacity: animation, child: child);
      }));
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return messageAlert(tipo: "esquelaFinalizada",);
        },useRootNavigator: false,
        barrierDismissible: false,
      );
    } else {
      print("La esquela no pudo ser Finalizada");
    }
  }
}
