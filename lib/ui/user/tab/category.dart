import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/company.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class CategoryPage extends StatefulWidget {

  String page, esquela_id;
  CategoryPage({Key key, this.page, this.esquela_id}) : super(key: key);
  @override
  _CategoryPageState createState() => new _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  ApiServices objApiService = ApiServices();
  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: widget.page=="1"?false:true,
        centerTitle: true,
        title: Text("CATEGORIAS"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Container(
          height: responsive.heigth,
          child: FutureBuilder(
            future: objApiService.getListCategory(),
            builder: (BuildContext context, AsyncSnapshot snapshot){
              if (!snapshot.hasData) {
                return Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ));
              }else if (snapshot.data.length == 0) {
                return Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 14,
                                ),
                                Text("No se encontraron categorias")
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              }else{
                return GridView.count (
                  crossAxisCount: 2,
                  children: List.generate (snapshot.data.length, (index) {
                    String id = snapshot.data[index]["id"].toString();
                    String img = snapshot.data[index]["img"].toString();
                    String titulo = snapshot.data[index]["titulo"];
                    return Container (
                        child: GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => CompanyPage(page: widget.page, id: id, esquela_id: widget.esquela_id),));
                          },
                          child: Card(
                            elevation: 0,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: responsive.wp(28),
                                  height: responsive.wp(28),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black,
                                        width: 1.0,
                                      ),
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: CachedNetworkImageProvider(img),
                                          fit: BoxFit.fill
                                      )
                                  ),
                                ),
                                SizedBox(height: 10,),
                                Text(titulo, textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(2), fontWeight: FontWeight.bold),),
                              ],
                            ),
                          ),
                        )
                    );
                  }),
                );
              }
            },
          ),
        )
      )
    );
  }
}