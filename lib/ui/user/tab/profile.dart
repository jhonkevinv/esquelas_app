import 'package:esquelas_app/model/user_model.dart';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/list_esquela.dart';
import 'package:esquelas_app/ui/user/page/purchase_history.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProfilePage extends StatefulWidget {

  @override
  _ProfilePageState createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  ApiServices objApiServices = ApiServices();
  UserModel _user = new UserModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Text("Editar perfil"),
      ),
      body: editProfile(context),
    );
  }

  Widget editProfile(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        Container(
          height: responsive.hp(31),
          width: responsive.width,
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(90), bottomRight: Radius.circular(90)),
              border: Border.all(
                color: Colors
                    .black, //                   <--- border color
                width: 1.5,
              ),
              image: DecorationImage(
                  image: CachedNetworkImageProvider(currentUser.value.img),
                  fit: BoxFit.cover
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 30, left: 30, bottom: 20),
          child: Column(
            children: <Widget>[
              SizedBox(height: responsive.hp(2.5),),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            initialValue: currentUser.value.nombre,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Nombre" : null,
                            onSaved: (input) => _user.nombre = input,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.person, color: Colors.black,),
                              labelText: "Nombre",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            initialValue: currentUser.value.apellido,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Apellido" : null,
                            onSaved: (input) => _user.apellido = input,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.person, color: Colors.black,),
                              labelText: "Apellido",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            initialValue: currentUser.value.email,
                            keyboardType: TextInputType.emailAddress,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar correo" : null,
                            onSaved: (input) => _user.email = input,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.email, color: Colors.black,),
                              labelText: "Correo",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            initialValue: currentUser.value.tel,
                            keyboardType: TextInputType.phone,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Teléfono" : null,
                            onSaved: (input) => _user.tel = input,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.phone_in_talk, color: Colors.black,),
                              labelText: "Teléfono",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            initialValue: currentUser.value.direccion,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar dirección" : null,
                            onSaved: (input) => _user.direccion = input,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.location_on, color: Colors.black,),
                              labelText: "Dirección",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: responsive.hp(3)),
              ButtonTheme(
                height: responsive.hp(6.3),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () {
                    if (_formKey.currentState
                        .validate()) {
                      _formKey.currentState.save();
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text('Cargando ...'),
                        duration: Duration(seconds: 3),
                        backgroundColor: Color(0XFF707070),
                      ));
                      UpdateUser();
                      print(_user);
                    }
                  },
                  textColor: Colors.white,
                  color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: Text(
                      "GUARDAR",
                      style: TextStyle(fontSize: responsive.dp(1.8)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
  UpdateUser() async {
    int status = 0;
    status = await objApiServices.updateUser(
      currentUser.value.id.toString(),
      _user.nombre,
      _user.apellido,
      _user.email,
      _user.tel,
      _user.direccion,
      currentUser.value.img
    );
    if (status == 1) {
      int statusload = 0;
      statusload = await objApiServices.getLoadDataUser(
        currentUser.value.id.toString(),
      );
      print(currentUser.value.id.toString());
      if(statusload == 1){
        print("datos actualizados");
      }else{
        print("datos no actualizados");
      }
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Datos Modificados'),
        duration: Duration(seconds: 3),
      ));
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Datos no Modificados'),
        duration: Duration(seconds: 3),
      ));
    }
  }
}