import 'dart:io';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/page/card_esquela.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';

class AddEsquelaPage extends StatefulWidget {

  @override
  _AddEsquelaPageState createState() => new _AddEsquelaPageState();
}

class _AddEsquelaPageState extends State<AddEsquelaPage> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  ApiServices objApiServices = ApiServices();
  TextEditingController nombreText = TextEditingController();
  TextEditingController apellidoText = TextEditingController();
  TextEditingController tituloText = TextEditingController();
  TextEditingController descripcionText = TextEditingController();
  String fechaFuneraria = 'año-mes-dia';
  String fechaCementerio = 'año-mes-dia';
  String horaVelorio = 'hh:mm:ss';
  String horaEntierro = 'hh:mm:ss';

  String funeraria = "";

  String cementerio = "";
  String id_img = "nada";

  File foto;
  int statusimg = 0;
  String init_foto = "https://p.kindpng.com/picc/s/21-211168_transparent-person-icon-png-png-download.png";
  bool monVal2 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text("AGREGAR ESQUELA"),
      ),
      body: addProfile(context),
    );
  }

  Widget addProfile(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        Container(
          height: responsive.hp(31),
          width: responsive.width,
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(90), bottomRight: Radius.circular(90)),
              border: Border.all(
                color: Colors.black, //                   <--- border color
                width: 1.5,
              ),
              image: DecorationImage(
                  image: statusimg == 0 ?NetworkImage(init_foto): FileImage(foto),
                  fit: BoxFit.cover
              )),
          child: Stack(
            children: <Widget>[
              Positioned(
                  bottom: 8,
                  right: 12,
                  child: GestureDetector(
                      onTap: _SelectImage,
                      child: Container(
                        height: responsive.wp(10),
                        width: responsive.wp(10),
                        child: Icon(
                          Icons.edit,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.black,
                            shape: BoxShape.circle
                        ),
                      )))
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 30, left: 30, bottom: 20),
          child: Column(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Nombre" : null,
                            controller: nombreText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Nombre",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Apellido" : null,
                            controller: apellidoText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Apellido",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Titulo" : null,
                            controller: tituloText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Titulo",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Descripción" : null,
                            controller: descripcionText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Descripción",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          //width: responsive.width,
                          child: FutureBuilder(
                              future:
                              objApiServices.getListCompanyTotal("2"),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      child: Center(
                                        child:
                                        CircularProgressIndicator(),
                                      ));
                                }
                                if (snapshot.data.length == 0) {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Funeraria",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        funeraria = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Funeraria" : null,
                                  );
                                } else {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Funeraria",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        funeraria = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Funeraria" : null,
                                  );
                                }
                              }),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          //width: responsive.width,
                          child: FutureBuilder(
                              future:
                              objApiServices.getListCompanyTotal("3"),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      child: Center(
                                        child:
                                        CircularProgressIndicator(),
                                      ));
                                }
                                if (snapshot.data.length == 0) {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Cementerio",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        cementerio = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Cementerio" : null,
                                  );
                                } else {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Cementerio",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        cementerio = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar Cementerio" : null,
                                  );
                                }
                              }),
                        )
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Fecha de velorio", style: TextStyle(fontSize: responsive.dp(1.6)), textAlign: TextAlign.start,),
                              SizedBox(height: responsive.hp(0.5),),
                              Container(
                                height: responsive.hp(6),
                                width: responsive.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30.0),
                                  border: Border.all(
                                    color: Colors.black, //                   <--- border color
                                    width: 1.0,
                                  ),
                                ),
                                child: InkWell(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(fechaFuneraria,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color(0xFF000000),
                                              fontSize: responsive.dp(1.6))),
                                    ],
                                  ),
                                  onTap: () {
                                    _selectDate(context);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 10,),
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Fecha de entierro", style: TextStyle(fontSize: responsive.dp(1.6)), textAlign: TextAlign.start,),
                              SizedBox(height: responsive.hp(0.5),),
                              Container(
                                height: responsive.hp(6),
                                width: responsive.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30.0),
                                  border: Border.all(
                                    color: Colors.black, //                   <--- border color
                                    width: 1.0,
                                  ),
                                ),
                                child: InkWell(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(fechaCementerio,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color(0xFF000000),
                                              fontSize: responsive.dp(1.6))),
                                    ],
                                  ),
                                  onTap: () {
                                    _selectDate2(context);
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Hora de velorio", style: TextStyle(fontSize: responsive.dp(1.6)), textAlign: TextAlign.start,),
                              SizedBox(
                                height: 3,
                              ),
                              Container(
                                width: responsive.width,
                                height: responsive.hp(6),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(30.0)),
                                  border: Border.all(
                                      color: Colors.black,
                                      width: 1.0),
                                ),
                                child: InkWell(
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(horaVelorio,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color(0xFF000000),
                                              fontSize: responsive.dp(1.6))),
                                    ],
                                  ),
                                  onTap: () {
                                    _selectDate3(context);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(width: 10,),
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Hora de entierro", style: TextStyle(fontSize: responsive.dp(1.6)), textAlign: TextAlign.start,),
                              SizedBox(
                                height: 3,
                              ),
                              Container(
                                width: responsive.width,
                                height: responsive.hp(6),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(30.0)),
                                  border: Border.all(
                                      color: Colors.black,
                                      width: 1.0),
                                ),
                                child: InkWell(
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(horaEntierro,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color(0xFF000000),
                                              fontSize: responsive.dp(1.6))),
                                    ],
                                  ),
                                  onTap: () {
                                    _selectDate4(context);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text("Fondos de esquelas", style: TextStyle(fontSize: responsive.dp(1.6)), textAlign: TextAlign.start,),
                        ),
                      ],
                    ),
                    SizedBox(height: 3,),
                    Container(
                      height: responsive.hp(15),
                      child: FutureBuilder(
                        future: objApiServices.getListImgEsquela(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Container(
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ));
                          }else if (snapshot.data.length == 0) {
                            return Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Flexible(
                                        child: Column(
                                          children: <Widget>[
                                            SizedBox(
                                              height: 14,
                                            ),
                                            Text("No hay fondos")
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            );
                          }else{
                            return ListView.builder(
                              scrollDirection: Axis.horizontal,
                                itemCount: snapshot.data.length,
                                itemBuilder: (context, index){
                                  String link = snapshot.data[index]["link"].toString();
                                  String id = snapshot.data[index]["id"].toString();
                                  return Padding(
                                    padding: const EdgeInsets.only(left: 5, right: 5),
                                    child: GestureDetector(
                                      onTap: (){
                                        setState(() {
                                          id_img = id;
                                        });
                                      },
                                      child: Container(
                                        width: responsive.wp(40),
                                        height: responsive.hp(15),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(20),
                                            border: Border.all(
                                              color: Colors
                                                  .black, //                   <--- border color
                                              width: 1.5,
                                            ),
                                            image: DecorationImage(
                                                image: NetworkImage(link),
                                                fit: BoxFit.cover
                                            )
                                        ),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            id_img == id
                                                ?Icon(Icons.check_circle_outline, color: Colors.green, size: responsive.dp(10),)
                                                :Container(),
                                          ],
                                        )
                                      ),
                                    )
                                  );
                                }
                            );
                          }
                        },
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: responsive.hp(3)),
              ButtonTheme(
                height: responsive.hp(6.3),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () {
                    if (_formKey.currentState
                        .validate()) {
                      if ((fechaFuneraria == "año-mes-dia") || (fechaCementerio == "año-mes-dia")) {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Seleccionar fecha'),
                          duration: Duration(seconds: 3),
                          backgroundColor: Color(0XFF707070),
                        ));
                      }else if((horaVelorio == "hh:mm:ss")||(horaEntierro == "hh:mm:ss")) {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Seleccionar hora'),
                          duration: Duration(seconds: 3),
                          backgroundColor: Color(0XFF707070),
                        ));
                      }else if((id_img == "nada")) {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Seleccionar fondo de imagen'),
                          duration: Duration(seconds: 3),
                          backgroundColor: Color(0XFF707070),
                        ));
                      }else{
                        if(foto != null){
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text('Cargando ...'),
                            duration: Duration(seconds: 3),
                            backgroundColor: Color(0XFF707070),
                          ));
                          Navigator.push(context, MaterialPageRoute(builder: (context) => CardEsquela(id_img: id_img, foto: foto, init_foto: init_foto, tipo: "sin foto", nombre: nombreText.text, apellido: apellidoText.text, titulo: tituloText.text, descripcion: descripcionText.text, funeraria: funeraria, cementerio: cementerio, fechaFuneraria: fechaFuneraria, horaVelorio: horaVelorio, fechaCementerio: fechaCementerio, horaEntierro: horaEntierro,),));
                        }else{
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text('Cargando'),
                            duration: Duration(seconds: 3),
                          ));
                          Navigator.push(context, MaterialPageRoute(builder: (context) => CardEsquela(id_img: id_img, foto: foto, init_foto: init_foto, tipo: "foto", nombre: nombreText.text, apellido: apellidoText.text, titulo: tituloText.text, descripcion: descripcionText.text, funeraria: funeraria, cementerio: cementerio, fechaFuneraria: fechaFuneraria, horaVelorio: horaVelorio, fechaCementerio: fechaCementerio, horaEntierro: horaEntierro,),));
                        }
                      }
                    }
                  },
                  textColor: Colors.white,
                  color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: Text(
                      "GUARDAR",
                      style: TextStyle(fontSize: responsive.dp(1.8)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Color(0xFFE38C8C),
              accentColor: Color(0xFFE38C8C),
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
    );
    if (d != null)
      setState(() {
        fechaFuneraria = new DateFormat('yyyy-MM-dd').format(d);
      });
  }
  Future<void> _selectDate2(BuildContext context) async {
    final DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Color(0xFFE38C8C),
              accentColor: Color(0xFFE38C8C),
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
      selectableDayPredicate: (DateTime day) {
        return day.difference(DateTime.now()).inDays < 30;
      },
    );
    if (d != null)
      setState(() {
        fechaCementerio = new DateFormat('yyyy-MM-dd').format(d);
      });
  }

  Future<void> _selectDate3(BuildContext context) async {
    final t = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: Colors.black,
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
    );
    if (t != null)
      setState(() {
        String modif1 = t.toString();
        String modif2 = modif1.split('(')[1];
        horaVelorio = modif2.split(')')[0]+":00";
      });
  }

  Future<void> _selectDate4(BuildContext context) async {
    final TimeOfDay t = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: Colors.black,
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
    );
    if (t != null)
      setState(() {
        String modif1 = t.toString();
        String modif2 = modif1.split('(')[1];
        horaEntierro = modif2.split(')')[0]+":00";
      });
  }

  _SelectImage() async{
    String estado = "";
    foto = await ImagePicker.pickImage(source: ImageSource.gallery);

    if(foto != null ){
      setState(() {
        statusimg = 1;
      });
    }
  }
}