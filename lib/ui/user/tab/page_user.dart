import 'package:esquelas_app/ui/user/tab/add_esquela.dart';
import 'package:esquelas_app/ui/user/tab/category.dart';
import 'package:esquelas_app/ui/user/tab/home.dart';
import 'package:esquelas_app/ui/user/tab/profile_user.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class PageUserWidget extends StatefulWidget {
  int page;
  PageUserWidget({Key key, this.page}) : super(key: key);
  @override
  _PageUserWidgetState createState() => _PageUserWidgetState();
}

class _PageUserWidgetState extends State<PageUserWidget> {
  GlobalKey _bottomNavigationKey = GlobalKey();

  initState() {
    super.initState();
    _selectTab(widget.page);
  }

  _selectTab(int pos) {
    switch (pos) {
      case 0:
        return HomePage();
      case 1:
        return AddEsquelaPage();
      case 2:
        return CategoryPage(page: "1");
      case 3:
        return ProfileUserPage();
      default:
        return Text("Error");
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            index: widget.page,
            height: 50.0,
            items: <Widget>[
              Icon(Icons.home, size: 30, color: Colors.white,),
              Icon(Icons.add, size: 30, color: Colors.white,),
              Icon(Icons.shopping_cart, size: 30, color: Colors.white,),
              Icon(Icons.person, size: 30, color: Colors.white,),
            ],
            color: Colors.black,
            buttonBackgroundColor: Colors.black,
            backgroundColor: Colors.transparent,
            animationCurve: Curves.easeInOut,
            animationDuration: Duration(milliseconds: 600),
            onTap: (index) {
              setState(() {
                widget.page = index;
              });
            },
          ),
          body: _selectTab(widget.page)
      ),
    );
  }
}