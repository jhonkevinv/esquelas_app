import 'package:esquelas_app/model/user_model.dart';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/tab/page_admin.dart';
import 'package:esquelas_app/ui/register_user.dart';
import 'package:esquelas_app/ui/user/tab/page_user.dart';
import 'package:esquelas_app/util/colors.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoginWidget extends StatefulWidget {
  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  bool _passwordVisible = false;
  UserModel _user= new UserModel();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ApiServices objApiServices = ApiServices();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        body: ListView(
          children: <Widget>[
            Container(
              height: responsive.hp(34),
              width: responsive.width,
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(70), bottomRight: Radius.circular(70)),
                  image: DecorationImage(
                      image: AssetImage("assets/img/cielo.jpeg"),
                      fit: BoxFit.cover
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, right: 30, left: 30, bottom: 20),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("LOGIN", textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(3)),),
                      )
                    ],
                  ),
                  SizedBox(height: responsive.hp(2),),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (input) => _user.email = input,
                                validator: (input) =>
                                input.length == 0 ? "Ingresar Correo" : null,
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(0),
                                  prefixIcon: Icon(Icons.email, color: primaryColorFont,),
                                  labelText: "Correo",
                                  labelStyle: TextStyle(color: Colors.black),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: primaryColorFont,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: TextFormField(
                                onChanged: (input) => _user.pass = input,
                                obscureText: !_passwordVisible,
                                validator: (input) => input.length == 0
                                    ? "Ingresar contraseña"
                                    : null,
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(0),
                                  prefixIcon: Icon(Icons.enhanced_encryption, color: primaryColorFont,),
                                  labelText: "Contraseña",
                                  labelStyle: TextStyle(color: Colors.black),
                                  fillColor: Colors.black,
                                  border: InputBorder.none,
                                  suffixIcon: IconButton(
                                    icon: _passwordVisible
                                        ? Icon(
                                      Icons.visibility_off,
                                      color: Colors.black45,
                                    )
                                        : Icon(
                                      Icons.visibility,
                                      color: Colors.black45,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _passwordVisible = !_passwordVisible;
                                      });
                                    },
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: primaryColorFont,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: responsive.hp(1.5)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("¿Olvidaste tu contraseña?", textAlign: TextAlign.end, style: TextStyle(color: primaryColorFont),),
                      )
                    ],
                  ),
                  SizedBox(height: responsive.hp(1.5)),
                  ButtonTheme(
                    minWidth: responsive.wp(100),
                    height: responsive.hp(6.3),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      onPressed: () {
                        if (_formKey.currentState
                            .validate()) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text('Validando ...'),
                            duration: Duration(seconds: 3),
                            backgroundColor: Color(0XFF707070),
                          ));
                          Login();
                        }
                      },
                      textColor: Colors.white,
                      color: Colors.black,
                      child: Text(
                        "Iniciar Sesión",
                        style: TextStyle(fontSize: responsive.dp(1.8)),
                      ),
                    ),
                  ),
                  SizedBox(height: responsive.hp(2)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterUserWidget(),));
                          },
                          child: Text("¿NUEVO USUARIO? REGISTRATE AHORA", textAlign: TextAlign.end, style: TextStyle(color: primaryColorFont),),
                        )
                      )
                    ],
                  ),
                  SizedBox(height: responsive.hp(5)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: GestureDetector(
                          onTap: (){
                            print(currentUser.value.nombre);
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                child: Lottie.asset('assets/lottie/facebook_lottie.json',
                                    width: responsive.wp(23), height: responsive.wp(23)),
                              ),
                            ],
                          ),
                        )
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Text("o"),
                            Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15),
                              child: Divider(color: Colors.black),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Lottie.asset('assets/lottie/google_lottie.json',
                                  width: responsive.wp(23), height: responsive.wp(23)),
                            ),
                          ],
                        )
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      )
    );
  }

  Login() async {
    int status = 0;
    status = await objApiServices.login(
      _user.email,
      _user.pass
    );
    if (status == 1) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => PageUserWidget(page: 0,),));
    } else if (status == 2) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => PageAdminWidget(page: 0),));
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Correo y contraseña incorrecto'),
        duration: Duration(seconds: 3),
      ));
    }
  }
}
