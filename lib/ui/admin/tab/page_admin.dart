import 'package:esquelas_app/ui/admin/tab/category.dart';
import 'package:esquelas_app/ui/admin/tab/home.dart';
import 'package:esquelas_app/ui/admin/tab/profile_admin.dart';
import 'package:esquelas_app/ui/admin/tab/search_date.dart';
import 'package:esquelas_app/ui/admin/tab/statistic.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class PageAdminWidget extends StatefulWidget {
  int page;
  PageAdminWidget({Key key, this.page}) : super(key: key);
  @override
  _PageAdminWidgetState createState() => _PageAdminWidgetState();
}

class _PageAdminWidgetState extends State<PageAdminWidget> {
  GlobalKey _bottomNavigationKey = GlobalKey();

  initState() {
    super.initState();
    _selectTab(widget.page);
  }

  _selectTab(int pos) {
    switch (pos) {
      case 0:
        return HomePage();
      case 1:
        return SearchDatePage();
      case 2:
        return StatisticPage();
      case 3:
        return CategoryPage();
      case 4:
        return ProfileAdminPage();
      default:
        return Text("Error");
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            index: widget.page,
            height: 50.0,
            items: <Widget>[
              Icon(Icons.home, size: 30, color: Colors.white,),
              Icon(Icons.calendar_today, size: 30, color: Colors.white,),
              Icon(Icons.trending_up, size: 30, color: Colors.white,),
              Icon(Icons.shopping_cart, size: 30, color: Colors.white,),
              Icon(Icons.person, size: 30, color: Colors.white,),
            ],
            color: Colors.black,
            buttonBackgroundColor: Colors.black,
            backgroundColor: Colors.transparent,
            animationCurve: Curves.easeInOut,
            animationDuration: Duration(milliseconds: 600),
            onTap: (index) {
              setState(() {
                widget.page = index;
              });
            },
          ),
          body: _selectTab(widget.page)
      ),
    );
  }
}