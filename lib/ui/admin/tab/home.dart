import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/esquela.dart';
import 'package:esquelas_app/ui/admin/page/search.dart';
import 'package:esquelas_app/ui/admin/page/validate_esquela.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ApiServices objApiService = ApiServices();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text("ESQUELAS"),
        leading: IconButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => ValidateEsquelaWidget(),));
          },
          icon: Icon(
            Icons.playlist_add_check,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => SearchWidget(),));
            },
          )
        ],
      ),
      body: esquelas(context),
    );
  }

  Widget esquelas(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return Container(
      height: responsive.heigth,
      child: FutureBuilder(
        future: objApiService.getListEsquela(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if (!snapshot.hasData) {
            return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ));
          }else if (snapshot.data.length == 0) {
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 14,
                            ),
                            Text("No hay datos")
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            );
          } else{
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index){
                  var img = snapshot.data[index]["img"];
                  String id = snapshot.data[index]["id"].toString();
                  String nombre = snapshot.data[index]["nombre"];
                  String apellido = snapshot.data[index]["apellido"];
                  String titulo = snapshot.data[index]["titulo"];
                  String descripcion = snapshot.data[index]["descripcion"];
                  String fecha_ini = snapshot.data[index]["fecha_ini"];
                  String fecha_fin = snapshot.data[index]["fecha_fin"];
                  String hora_ini = snapshot.data[index]["hora_ini"];
                  String hora_fin = snapshot.data[index]["hora_fin"];
                  String fecha_ini_entierro = snapshot.data[index]["fecha_ini_entierro"];
                  String hora_ini_entierro = snapshot.data[index]["hora_ini_entierro"];
                  String hora_fin_entierro = snapshot.data[index]["hora_fin_entierro"];
                  String funeraria_empresas_id = snapshot.data[index]["funeraria_empresas_id"].toString();
                  String cementerio_empresa_id = snapshot.data[index]["cementerio_empresa_id"].toString();
                  String estado = snapshot.data[index]["estado"].toString();
                  String fondo_img_id = snapshot.data[index]["fondo_img_id"].toString();
                  if(estado != "1"){
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => EsquelaWidget(id: id,img: img, nombre: nombre, apellido: apellido, titulo: titulo, descripcion: descripcion, fecha_ini: fecha_ini, fecha_fin: fecha_fin, hora_ini: hora_ini, hora_fin: hora_fin, fecha_ini_entierro: fecha_ini_entierro, hora_ini_entierro: hora_ini_entierro, hora_fin_entierro: hora_fin_entierro, cementerio_empresa_id: cementerio_empresa_id, funeraria_empresas_id: funeraria_empresas_id, estado: estado, page: "home"),));
                      },
                      child: Container(
                        height: responsive.hp(30),
                        child: FutureBuilder(
                          future: objApiService.getListImgEsquela(),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return Container(
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ));
                            }else if (snapshot.data.length == 0) {
                              return Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Flexible(
                                          child: Column(
                                            children: <Widget>[
                                              SizedBox(
                                                height: 14,
                                              ),
                                              Text("No hay datos")
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            }else{
                              return ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (context, index){
                                    String link = snapshot.data[index]["link"].toString();
                                    String id = snapshot.data[index]["id"].toString();
                                    if(fondo_img_id == id){
                                      return Card(
                                          shape: RoundedRectangleBorder(
                                            side: BorderSide(color: Colors.white70, width: 1),
                                            borderRadius: BorderRadius.circular(40),
                                          ),
                                          margin: EdgeInsets.all(10.0),
                                          elevation: 0,
                                          child: Container(
                                            height: responsive.hp(28),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(40),
                                              border: Border.all(
                                                color: Colors
                                                    .black, //                   <--- border color
                                                width: 1.5,
                                              ),
                                              image: DecorationImage(
                                                image: CachedNetworkImageProvider(link),
                                                fit: BoxFit.fill,
                                                colorFilter: ColorFilter.mode(
                                                    Colors.grey.withOpacity(1),
                                                    BlendMode.softLight
                                                ),
                                              ),
                                            ),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Expanded(
                                                    flex: 1,
                                                    child: Padding(
                                                        padding: const EdgeInsets.all(20),
                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: <Widget>[
                                                            Container(
                                                                decoration: BoxDecoration(
                                                                    borderRadius: BorderRadius.circular(20),
                                                                    color: Colors.black
                                                                ),
                                                                child: Padding(
                                                                  padding: const EdgeInsets.all(5),
                                                                  child: Column(
                                                                    children: <Widget>[
                                                                      if (estado=="1")
                                                                        Text("NO VALIDADO", style: TextStyle(fontSize: responsive.dp(1.8), color: Colors.white, fontWeight: FontWeight.bold))
                                                                      else if (estado=="2")
                                                                        Text("VALIDADO", style: TextStyle(fontSize: responsive.dp(1.8), color: Colors.white, fontWeight: FontWeight.bold))
                                                                      else if (estado=="3")
                                                                          Text("FINALIZADO", style: TextStyle(fontSize: responsive.dp(1.8), color: Colors.white, fontWeight: FontWeight.bold))
                                                                        else if (estado=="0")
                                                                            Text("CANCELADO", style: TextStyle(fontSize: responsive.dp(1.8), color: Colors.white, fontWeight: FontWeight.bold))
                                                                    ],
                                                                  ),
                                                                )
                                                            )
                                                          ],
                                                        )
                                                    )
                                                ),
                                                Expanded(
                                                    flex: 1,
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(top: 20, bottom: 20, right: 30),
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: <Widget>[
                                                          Container(
                                                            width: responsive.wp(25),
                                                            height: responsive.wp(25),
                                                            decoration: BoxDecoration(
                                                                shape: BoxShape.circle,
                                                                image: DecorationImage(
                                                                    image: CachedNetworkImageProvider(img),
                                                                    fit: BoxFit.fill
                                                                )
                                                            ),
                                                          ),
                                                          SizedBox(height: 10,),
                                                          Text(nombre, textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(2.2), color: Colors.black, fontWeight: FontWeight.bold), maxLines: 1,),
                                                          Text(apellido, textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(2.2), color: Colors.black, fontWeight: FontWeight.bold), maxLines: 1,),
                                                        ],
                                                      ),
                                                    )
                                                )
                                              ],
                                            ),
                                          )
                                      );
                                    }else{
                                      return Container();
                                    }
                                  }
                              );
                            }
                          },
                        ),
                      )
                    );
                  }else{
                    return Container();
                  }
                }
            );
          }
        },
      ),
    );
  }
}