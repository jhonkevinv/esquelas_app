import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';

class StatisticPage extends StatefulWidget {
  @override
  _StatisticPageState createState() => _StatisticPageState();
}

class _StatisticPageState extends State<StatisticPage> {

  String fechaInicio = 'dia / mes / año';
  String fechaFin = 'dia / mes / año';
  String fechaInicioProduct = 'dia / mes / año';
  String fechaFinProduct = 'dia / mes / año';

  final List<PopulationDataEsquela> dataEsquela = [
    PopulationDataEsquela(
        esquela: "Esquela",
        cantidad: 50,
    ),
    PopulationDataEsquela(
        esquela: "Esquela 2",
        cantidad: 20,
    ),
    PopulationDataEsquela(
        esquela: "Esquela 3",
        cantidad: 60,
    ),
  ];

  final List<DataUser> datauser = [
    DataUser(
      tipo_user: "Usuario",
      cantidad: 50,
    ),
    DataUser(
      tipo_user: "Empresa",
      cantidad: 10,
    ),
    DataUser(
      tipo_user: "Administrador",
      cantidad: 4,
    ),
  ];

  final List<DataProduct> dataproduct = [
    DataProduct(
      product: "Flor1",
      cantidad: 90,
    ),
    DataProduct(
      product: "Flor2",
      cantidad: 40,
    ),
    DataProduct(
      product: "Flor3",
      cantidad: 70,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("ESTADISTICAS"),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(height: responsive.hp(3),),
              Text(
                "Esquelas Publicadas",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: responsive.dp(2)),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20, left: 20, top: 20, bottom: 10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Text("Fecha Inicio", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Container(
                            height: responsive.hp(4),
                            width: responsive.wp(35),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              border: Border.all(
                                color: Colors.black, //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: InkWell(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(fechaInicio,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color(0xFF000000),
                                          fontSize: 10)),
                                ],
                              ),
                              onTap: () {
                                _selectDate(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Text("Fecha Fin", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Container(
                            height: responsive.hp(4),
                            width: responsive.wp(35),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              border: Border.all(
                                color: Colors.black, //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: InkWell(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(fechaFin,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color(0xFF000000),
                                          fontSize: 10)),
                                ],
                              ),
                              onTap: () {
                                _selectDate2(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: 300,
            padding: EdgeInsets.all(20),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: charts.BarChart(
                      _getSeriesDataEsquela(),
                      animate: true,
                      domainAxis: charts.OrdinalAxisSpec(
                          renderSpec: charts.SmallTickRendererSpec(labelRotation: 60)
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Column(
            children: <Widget>[
              //SizedBox(height: responsive.hp(3),),
              Text(
                "Usuarios Nuevos",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: responsive.dp(2)),
              ),
            ],
          ),
          Container(
            height: 300,
            padding: EdgeInsets.all(20),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: charts.BarChart(
                      _getDataUser(),
                      animate: true,
                      domainAxis: charts.OrdinalAxisSpec(
                          renderSpec: charts.SmallTickRendererSpec(labelRotation: 60)
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Text(
                "Productos mas vendidos",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: responsive.dp(2)),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20, left: 20, top: 20, bottom: 10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Text("Fecha Inicio", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Container(
                            height: responsive.hp(4),
                            width: responsive.wp(35),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              border: Border.all(
                                color: Colors.black, //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: InkWell(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(fechaInicioProduct,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color(0xFF000000),
                                          fontSize: 10)),
                                ],
                              ),
                              onTap: () {
                                _selectDateProduct(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Text("Fecha Fin", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Container(
                            height: responsive.hp(4),
                            width: responsive.wp(35),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              border: Border.all(
                                color: Colors.black, //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: InkWell(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(fechaFinProduct,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color(0xFF000000),
                                          fontSize: 10)),
                                ],
                              ),
                              onTap: () {
                                _selectDateProduct2(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: 300,
            padding: EdgeInsets.all(20),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: charts.PieChart(
                      _getDataProduct(),
                      animate: true,
                      defaultRenderer: new charts.ArcRendererConfig(
                          arcWidth: 60,
                          arcRendererDecorators: [new charts.ArcLabelDecorator(
                            labelPosition: charts.ArcLabelPosition.inside
                          )]
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Color(0xFFE38C8C),
              accentColor: Color(0xFFE38C8C),
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
    );
    if (d != null)
      setState(() {
        fechaInicio = new DateFormat('dd/MM/yyyy').format(d);
      });
  }
  Future<void> _selectDate2(BuildContext context) async {
    final DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Color(0xFFE38C8C),
              accentColor: Color(0xFFE38C8C),
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
      selectableDayPredicate: (DateTime day) {
        return day.difference(DateTime.now()).inDays < 30;
      },
    );
    if (d != null)
      setState(() {
        fechaFin = new DateFormat('dd/MM/yyyy').format(d);
      });
  }

  Future<void> _selectDateProduct(BuildContext context) async {
    final DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Color(0xFFE38C8C),
              accentColor: Color(0xFFE38C8C),
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
    );
    if (d != null)
      setState(() {
        fechaInicio = new DateFormat('dd/MM/yyyy').format(d);
      });
  }
  Future<void> _selectDateProduct2(BuildContext context) async {
    final DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
              primaryColor: Color(0xFFE38C8C),
              accentColor: Color(0xFFE38C8C),
              scaffoldBackgroundColor: Colors.green,
              primarySwatch: Colors.blueGrey),
          child: child,
        );
      },
      selectableDayPredicate: (DateTime day) {
        return day.difference(DateTime.now()).inDays < 30;
      },
    );
    if (d != null)
      setState(() {
        fechaFin = new DateFormat('dd/MM/yyyy').format(d);
      });
  }

  _getSeriesDataEsquela() {
    List<charts.Series<PopulationDataEsquela, String>> series = [
      charts.Series(
          id: "Esquela",
          data: dataEsquela,
          domainFn: (PopulationDataEsquela series, _) => series.esquela,
          measureFn: (PopulationDataEsquela series, _) => series.cantidad,
          colorFn: (PopulationDataEsquela series, _) => charts.ColorUtil.fromDartColor(Colors.lightBlue),
      )
    ];
    return series;
  }

  _getDataUser() {
    List<charts.Series<DataUser, String>> series = [
      charts.Series(
        id: "User",
        data: datauser,
        domainFn: (DataUser series, _) => series.tipo_user,
        measureFn: (DataUser series, _) => series.cantidad,
        colorFn: (DataUser series, _) => charts.ColorUtil.fromDartColor(Colors.orange),
      )
    ];
    return series;
  }

  _getDataProduct() {
    List<charts.Series<DataProduct, String>> series = [
      charts.Series(
        id: "User",
        data: dataproduct,
        labelAccessorFn: (DataProduct series, _) => '${series.product}: ${series.cantidad}',
        domainFn: (DataProduct series, _) => series.product,
        measureFn: (DataProduct series, _) => series.cantidad,
        colorFn: (DataProduct series, _) => charts.ColorUtil.fromDartColor(Colors.green),
      )
    ];
    return series;
  }
}

class PopulationDataEsquela {
  String esquela;
  int cantidad;
  PopulationDataEsquela({
    @required this.esquela,
    @required this.cantidad,
  });
}

class DataUser {
  String tipo_user;
  int cantidad;
  DataUser({
    @required this.tipo_user,
    @required this.cantidad,
  });
}

class DataProduct {
  String product;
  int cantidad;
  DataProduct({
    @required this.product,
    @required this.cantidad,
  });
}
