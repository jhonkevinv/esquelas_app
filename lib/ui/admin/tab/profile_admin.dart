import 'package:esquelas_app/model/user_model.dart';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/admin_list.dart';
import 'package:esquelas_app/ui/admin/page/img_esquela.dart';
import 'package:esquelas_app/ui/admin/page/register_company.dart';
import 'package:esquelas_app/ui/admin/tab/profile.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProfileAdminPage extends StatefulWidget {

  @override
  _ProfileAdminPageState createState() => new _ProfileAdminPageState();
}

class _ProfileAdminPageState extends State<ProfileAdminPage> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  ApiServices objApiServices = ApiServices();
  UserModel _user = new UserModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text("PERFIL"),
      ),
      body: editProfile(context),
    );
  }

  Widget editProfile(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: 60),
                Column(
                  children: <Widget>[
                    Container(
                      height: responsive.wp(35),
                      width: responsive.wp(35),
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors
                                .grey, //                   <--- border color
                            width: 4,
                          ),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  currentUser.value.img)),
                          shape: BoxShape.circle,
                          color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        currentUser.value.nombre,
                        style: TextStyle(fontSize: responsive.dp(2), fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        currentUser.value.apellido,
                        style: TextStyle(fontSize: responsive.dp(2), fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                        currentUser.value.email,
                        style: TextStyle(fontSize: responsive.dp(1.6))
                    ),
                  ],
                ),
                SizedBox(height: 30),
                InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(),));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        right: 15, left: 15),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                        BorderRadius.all(
                            Radius.circular(
                                25.0)),
                      ),
                      child: Padding(
                        padding:
                        const EdgeInsets.all(10),
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.edit,
                                    ),
                                    SizedBox(
                                        width: 30),
                                    Text(
                                      'Editar perfil',
                                      style: TextStyle(fontSize: responsive.dp(1.6)),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .end,
                                  children: <Widget>[
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5),
                InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AdminListWidget(),));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        right: 15, left: 15),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                        BorderRadius.all(
                            Radius.circular(
                                25.0)),
                      ),
                      child: Padding(
                        padding:
                        const EdgeInsets.all(10),
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.person_outline,
                                    ),
                                    SizedBox(
                                        width: 30),
                                    Text(
                                      'Administradores',
                                      style: TextStyle(fontSize: responsive.dp(1.6)),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .end,
                                  children: <Widget>[
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(PageRouteBuilder(
                        pageBuilder: (BuildContext context, _, __) {
                          return RegisterCompanyWidget();
                        }, transitionsBuilder:
                        (_, Animation<double> animation, __, Widget child) {
                      return FadeTransition(opacity: animation, child: child);
                    }));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        right: 15, left: 15),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                        BorderRadius.all(
                            Radius.circular(
                                25.0)),
                      ),
                      child: Padding(
                        padding:
                        const EdgeInsets.all(10),
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.card_travel,
                                    ),
                                    SizedBox(
                                        width: 30),
                                    Text(
                                      'Registrar empresas',
                                      style: TextStyle(fontSize: responsive.dp(1.6)),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .end,
                                  children: <Widget>[
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5),
                InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ImgEsquelaWidget(),));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        right: 15, left: 15),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                        BorderRadius.all(
                            Radius.circular(
                                25.0)),
                      ),
                      child: Padding(
                        padding:
                        const EdgeInsets.all(10),
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.image,
                                    ),
                                    SizedBox(
                                        width: 30),
                                    Text(
                                      'Fondos de esquelas',
                                      style: TextStyle(fontSize: responsive.dp(1.6)),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .end,
                                  children: <Widget>[
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5),
                InkWell(
                  onTap: () async {
                    return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          // return object of type Dialog
                          return messagePermition(tipo: "session",);
                        },useRootNavigator: false,
                        barrierDismissible: false
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        right: 15, left: 15),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                        BorderRadius.all(
                            Radius.circular(
                                25.0)),
                      ),
                      child: Padding(
                        padding:
                        const EdgeInsets.all(10),
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.exit_to_app,
                                    ),
                                    SizedBox(
                                        width: 30),
                                    Text("Cerrar Sesión",
                                      style: TextStyle(fontSize: responsive.dp(1.6)),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ))
      ],
    );
  }
}