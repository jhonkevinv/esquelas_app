import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/search.dart';
import 'package:esquelas_app/ui/admin/page/validate_esquela.dart';
import 'package:esquelas_app/ui/admin/tab/page_admin.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:lottie/lottie.dart';

class CupertinoSwitchNotification extends StatefulWidget {
  String id;
  String statusNotification;
  String tipo;
  String page;

  CupertinoSwitchNotification(
      {Key key,
        this.statusNotification, this.id, this.tipo, this.page
      })
      : super(key: key);
  CupertinoSwitchNotificationState createState() =>
      CupertinoSwitchNotificationState();
}

class CupertinoSwitchNotificationState
    extends State<CupertinoSwitchNotification> {
  ApiServices objApiService = ApiServices();
  bool statusButton = false;

  @override
  void initState() {
    super.initState();
    if(widget.tipo == "esquela"){
      if ((widget.statusNotification == "1")||(widget.statusNotification == "2")||(widget.statusNotification == "3")) {
        statusButton = true;
      } else if (widget.statusNotification == "0") {
        statusButton = false;
      }
    }else{
      if (widget.statusNotification == "1") {
        statusButton = true;
      } else if (widget.statusNotification == "2") {
        statusButton = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Column(
      children: <Widget>[
        CupertinoSwitch(
          value: statusButton,
          onChanged: (bool value) {
            print(widget.statusNotification);
            if(widget.tipo == "administrador"){
              methodAsyncUser();
            }else if(widget.tipo == "empresa"){
              methodAsyncCompany();
            }else if(widget.tipo == "producto"){
              methodAsyncProduct();
            }else if(widget.tipo == "esquela"){
              methodAsyncEsquela();
            }else{
              print("no encontrado");
            }
          },
          trackColor: Colors.grey,
          activeColor: Colors.green,
        ),
        (widget.tipo == "esquela") && (widget.statusNotification=="2")
            ?ButtonTheme(
            height: responsive.hp(6.3),
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              onPressed: () {
                methodAsyncEsquelaFinalize();
              },
              textColor: Colors.white,
              color: Colors.black,
              child: Padding(
                padding: const EdgeInsets.only(right: 10, left: 10),
                child: Text(
                  "Finalizar esquela",
                  style: TextStyle(fontSize: responsive.dp(1.8)),
                ),
              ),
            ),
          )
        :Container(),
        (widget.tipo == "esquela") && (widget.statusNotification=="1")
            ?ButtonTheme(
          height: responsive.hp(6.3),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            onPressed: () {
              return showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return messageAlertAdmin(id: widget.id, page: widget.page, tipo: "question");
                  },useRootNavigator: false,
                  barrierDismissible: false
              );
            },
            textColor: Colors.white,
            color: Colors.black,
            child: Padding(
              padding: const EdgeInsets.only(right: 10, left: 10),
              child: Text(
                "Validar esquela",
                style: TextStyle(fontSize: responsive.dp(1.8)),
              ),
            ),
          ),
        )
        :Container(),
      ],
    );
  }

  void methodAsyncUser() async {
    int status = await objApiService.updateStateUser(
      widget.id,
      widget.statusNotification,
      );
    if (status == 1) {
      print("cambiado");
      setState(() {
        if (statusButton == true) {
          statusButton = false;
          widget.statusNotification = "2";
        } else {
          statusButton = true;
          widget.statusNotification = "1";
        }
      });
    }else{
      print("no cambiado");
    }
  }

  void methodAsyncCompany() async {
    int status = await objApiService.updateStateCompany(
      widget.id,
      widget.statusNotification,
    );
    if (status == 1) {
      print("cambiado");
      setState(() {
        if (statusButton == true) {
          statusButton = false;
          widget.statusNotification = "2";
        } else {
          statusButton = true;
          widget.statusNotification = "1";
        }
      });
    }else{
      print("no cambiado");
    }
  }

  void methodAsyncProduct() async {
    int status = await objApiService.updateStateProduct(
      widget.id,
      widget.statusNotification,
    );
    if (status == 1) {
      print("cambiado");
      setState(() {
        if (statusButton == true) {
          statusButton = false;
          widget.statusNotification = "2";
        } else {
          statusButton = true;
          widget.statusNotification = "1";
        }
      });
    }else{
      print("no cambiado");
    }
  }

  void methodAsyncEsquela() async {
    int status = await objApiService.updateStateEsquela(
      widget.id,
      widget.statusNotification,
    );
    if (status == 1) {
      print("cambiado");
      setState(() {
        if (statusButton == true) {
          statusButton = false;
          widget.statusNotification = "0";
        } else {
          statusButton = true;
          widget.statusNotification = "1";
        }
      });
    }else{
      print("no cambiado");
    }
  }

  void methodAsyncEsquelaFinalize() async {
    int status = await objApiService.updateStateFinalizeEsquela(
      widget.id,
      "3",
    );
    if (status == 1) {
      print("cambiado");
      setState(() {
        if (statusButton == true) {
          statusButton = true;
          widget.statusNotification = "3";
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return messageAlertAdmin(id: widget.id, page: widget.page, tipo: "finalizado");
              },useRootNavigator: false,
              barrierDismissible: false
          );
        } else {
          statusButton = true;
          widget.statusNotification = "3";
        }
      });
    }else{
      print("no cambiado");
    }
  }
}

class messageAlertAdmin extends StatefulWidget {
  String id, page, tipo;
  messageAlertAdmin({Key key, this.id, this.page, this.tipo}) : super(key: key);
  @override
  _messageAlertAdminState createState() => _messageAlertAdminState();
}

class _messageAlertAdminState extends State<messageAlertAdmin> {
  ApiServices objApiService = ApiServices();
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive.of(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        title: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                if(widget.tipo=="question")
                Text("¿Desea validar la esquela?", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else if(widget.tipo=="aceptado")
                Text("Esquela validada con exito", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
                else
                Text("Esquela finalizada con exito", style: TextStyle(color: Colors.black, fontSize: responsive.dp(2.2)))
              ],
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                widget.tipo=="question"
                ?Container(
                  child: Lottie.asset('assets/lottie/question_lottie.json',
                      width: responsive.wp(90), height: responsive.hp(25)),
                )
                :Container(
                  child: Lottie.asset('assets/lottie/check_lottie.json',
                      width: responsive.wp(90), height: responsive.hp(25)),
                ),
                SizedBox(height: 10,),
                widget.tipo=="question"
                ?Row(
                  children: <Widget>[
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: responsive.wp(20),
                              height: 50,
                              buttonColor: Colors.black,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0)),
                                onPressed: (){
                                  Navigator.pop(context);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text("No",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                    ),
                    SizedBox(width: 20,),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          ButtonTheme(
                            minWidth: responsive.wp(20),
                            height: 50,
                            buttonColor: Colors.black,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25.0)),
                              onPressed: () async {
                                validateEsquela();
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text("Si",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
                :Row(
                  children: <Widget>[
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: responsive.wp(20),
                              height: 50,
                              buttonColor: Colors.black,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0)),
                                onPressed: (){
                                  Navigator.pop(context);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text("Aceptar",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  validateEsquela() async {
    int status = await objApiService.updateStateFinalizeEsquela(
      widget.id,
      "2",
    );
    if (status == 1) {
      print("cambiado");
      if(widget.page == "home"){
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return PageAdminWidget(page: 0,);
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return messageAlertAdmin(id: widget.id, page: widget.page, tipo: "aceptado");
            },useRootNavigator: false,
            barrierDismissible: false
        );
      }else if(widget.page == "search"){
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return SearchWidget();
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return messageAlertAdmin(id: widget.id, page: widget.page, tipo: "aceptado");
            },useRootNavigator: false,
            barrierDismissible: false
        );
      }else if(widget.page == "validate"){
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return ValidateEsquelaWidget();
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return messageAlertAdmin(id: widget.id, page: widget.page, tipo: "aceptado");
            },useRootNavigator: false,
            barrierDismissible: false
        );
      }else{
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return PageAdminWidget(page: 1,);
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return messageAlertAdmin(id: widget.id, page: widget.page, tipo: "aceptado");
            },useRootNavigator: false,
            barrierDismissible: false
        );
      }
    }else{
      print("no cambiado");
    }
  }
}