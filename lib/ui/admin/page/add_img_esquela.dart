import 'dart:io';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/img_esquela.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/colors.dart';
import 'package:image_picker/image_picker.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cached_network_image/cached_network_image.dart';

class AddImgEsquelaWidget extends StatefulWidget {
  @override
  _AddImgEsquelaWidgetState createState() => _AddImgEsquelaWidgetState();
}

class _AddImgEsquelaWidgetState extends State<AddImgEsquelaWidget> {

  String init_foto = "https://img.icons8.com/clouds/344/image.png";
  File foto;
  int statusimg = 0;
  TextEditingController titleText = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ApiServices objApiServices = ApiServices();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return ImgEsquelaWidget();
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.black,
          centerTitle: true,
          title: Text("Agregar fondo"),
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(PageRouteBuilder(
                  pageBuilder: (BuildContext context, _, __) {
                    return ImgEsquelaWidget();
                  }, transitionsBuilder:
                  (_, Animation<double> animation, __, Widget child) {
                return FadeTransition(opacity: animation, child: child);
              }));
            },
            icon: Icon(
              Icons.arrow_back,
            ),
          ),
        ),
        body: bodyWidget(),
      ),
    );
  }
  Widget bodyWidget(){
    Responsive responsive = Responsive.of(context);
    return Center(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Card(
                      margin: EdgeInsets.all(20.0),
                      elevation: 0,
                      child: Container(
                        height: responsive.hp(28),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(
                            color: Colors
                                .black, //                   <--- border color
                            width: 1.5,
                          ),
                          image: DecorationImage(
                            image: statusimg == 0 ?CachedNetworkImageProvider(init_foto): FileImage(foto),
                            fit: BoxFit.cover,
                          ),
                        ),
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                                bottom: 0,
                                right: 0,
                                child: GestureDetector(
                                    onTap: _SelectImage,
                                    child: Container(
                                      height: responsive.wp(10),
                                      width: responsive.wp(10),
                                      child: Icon(
                                        Icons.image,
                                        color: Colors.white,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          shape: BoxShape.circle
                                      ),
                                    )))
                          ],
                        ),
                      )
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          controller: titleText,
                          validator: (input) =>
                          input.length == 0 ? "Ingresar titulo" : null,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(0),
                            prefixIcon: Icon(Icons.person, color: Colors.black,),
                            labelText: "Titulo",
                            labelStyle: TextStyle(color: Colors.black),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: primaryColorFont,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20,),
                  ButtonTheme(
                    minWidth: responsive.wp(50),
                    height: responsive.hp(6.3),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      onPressed: () {
                        if (_formKey.currentState
                            .validate()) {
                          if(foto != null){
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text('Cargando ...'),
                              duration: Duration(seconds: 3),
                              backgroundColor: Color(0XFF707070),
                            ));
                            _updateImage();
                          }else{
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text('Agregar imagen'),
                              duration: Duration(seconds: 3),
                            ));
                          }
                        }
                      },
                      textColor: Colors.white,
                      color: Colors.black,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10, left: 10),
                        child: Text(
                          "Agregar",
                          style: TextStyle(fontSize: responsive.dp(1.8)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          )
        ],
      ),
    );
  }

  _SelectImage() async{
    foto = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(foto != null ){
      setState(() {
        statusimg = 1;
      });
    }
  }

  _updateImage() async{
    String estado = "";
    estado = await objApiServices.updatePhotoUser(foto);
    if(estado==""){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Imagen no cargada'),
        duration: Duration(seconds: 3),
      ));
    }else{
      Register(estado);
    }
  }

  Register(String img) async {
    int status = 0;
    status = await objApiServices.addImgEsquela(
        titleText.text,
        img
    );
    if (status == 1) {
      Navigator.of(context).pushReplacement(PageRouteBuilder(
          pageBuilder: (BuildContext context, _, __) {
            return ImgEsquelaWidget();
          }, transitionsBuilder:
          (_, Animation<double> animation, __, Widget child) {
        return FadeTransition(opacity: animation, child: child);
      }));
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return messageAlert(tipo: "fondo",);
        },useRootNavigator: false,
        barrierDismissible: false,
      );
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Usuario no registrado'),
        duration: Duration(seconds: 3),
      ));
    }
  }
}
