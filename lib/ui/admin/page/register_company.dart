import 'dart:io';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/tab/page_admin.dart';
import 'package:esquelas_app/ui/user/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cached_network_image/cached_network_image.dart';

class RegisterCompanyWidget extends StatefulWidget {

  @override
  _RegisterCompanyWidgetState createState() => new _RegisterCompanyWidgetState();
}

class _RegisterCompanyWidgetState extends State<RegisterCompanyWidget> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  ApiServices objApiServices = ApiServices();
  TextEditingController tituloText = TextEditingController();
  TextEditingController descripcionText = TextEditingController();
  TextEditingController direccionText = TextEditingController();
  TextEditingController telefonoText = TextEditingController();
  TextEditingController emailText = TextEditingController();
  TextEditingController passwordText = TextEditingController();
  bool _passwordVisible = false;

  String tipo = "";
  String categoria = "";

  File foto;
  int statusimg = 0;
  String init_foto = "https://img.icons8.com/clouds/2x/company.png";
  bool monVal2 = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              return PageAdminWidget(page: 4,);
            }, transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        }));
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Text("Agregar empresa"),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(PageRouteBuilder(
                  pageBuilder: (BuildContext context, _, __) {
                    return PageAdminWidget(page: 4,);
                  }, transitionsBuilder:
                  (_, Animation<double> animation, __, Widget child) {
                return FadeTransition(opacity: animation, child: child);
              }));
            },
            icon: Icon(
              Icons.arrow_back,
            ),
          ),
        ),
        body: addProfile(context),
      ),
    );
  }

  Widget addProfile(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 30, left: 30, bottom: 20),
          child: Column(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Card(
                        margin: EdgeInsets.all(20.0),
                        elevation: 0,
                        child: Container(
                          height: responsive.hp(28),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            border: Border.all(
                              color: Colors
                                  .black, //                   <--- border color
                              width: 1.5,
                            ),
                            image: DecorationImage(
                              image: statusimg == 0 ?CachedNetworkImageProvider(init_foto): FileImage(foto),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Stack(
                            children: <Widget>[
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: GestureDetector(
                                      onTap: _SelectImage,
                                      child: Container(
                                        height: responsive.wp(10),
                                        width: responsive.wp(10),
                                        child: Icon(
                                          Icons.image,
                                          color: Colors.white,
                                        ),
                                        decoration: BoxDecoration(
                                            color: Colors.black,
                                            shape: BoxShape.circle
                                        ),
                                      )))
                            ],
                          ),
                        )
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Titulo" : null,
                            controller: tituloText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Titulo",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Descripción" : null,
                            controller: descripcionText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Descripción",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar dirección" : null,
                            controller: direccionText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Dirección",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.phone,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar telefono" : null,
                            controller: telefonoText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Telefono",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            validator: (input) =>
                            input.length == 0 || !input.contains("@") ? "Ingresar correo" : null,
                            controller: emailText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Correo",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: passwordText,
                            obscureText: !_passwordVisible,
                            validator: (input) => input.length == 0
                                ? "Ingresar contraseña"
                                : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                              labelText: "Contraseña",
                              labelStyle: TextStyle(color: Colors.black),
                              fillColor: Colors.black,
                              border: InputBorder.none,
                              suffixIcon: IconButton(
                                icon: _passwordVisible
                                    ? Icon(
                                  Icons.visibility_off,
                                  color: Colors.black45,
                                )
                                    : Icon(
                                  Icons.visibility,
                                  color: Colors.black45,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _passwordVisible = !_passwordVisible;
                                  });
                                },
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          //width: responsive.width,
                          child: FutureBuilder(
                              future:
                              objApiServices.getListTipoEmpresa(),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      child: Center(
                                        child:
                                        CircularProgressIndicator(),
                                      ));
                                }
                                if (snapshot.data.length == 0) {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Tipo de empresa",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        tipo = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar tipo de empresa" : null,
                                  );
                                } else {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Tipo de empresa",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        tipo = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar tipo de empresa" : null,
                                  );
                                }
                              }),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          //width: responsive.width,
                          child: FutureBuilder(
                              future:
                              objApiServices.getListCategory(),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      child: Center(
                                        child:
                                        CircularProgressIndicator(),
                                      ));
                                }
                                if (snapshot.data.length == 0) {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Categoria",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        categoria = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar categoria" : null,
                                  );
                                } else {
                                  return DropdownButtonFormField<String>(
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.arrow_right, color: Colors.black,),
                                      contentPadding: const EdgeInsets.only(right: 10, left: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                        ),
                                      ),
                                      hintText: "Categoria",
                                      hintStyle: TextStyle(color: Colors.black),
                                    ),
                                    items: snapshot.data.map<DropdownMenuItem<String>>((item) {
                                      return DropdownMenuItem<String>(
                                        child: Text(
                                          item['titulo'].toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        value: item['id'].toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) async {
                                      setState(() {
                                        categoria = value;
                                      });
                                    },
                                    validator: (input) => input == null ? "Seleccionar categoria" : null,
                                  );
                                }
                              }),
                        )
                      ],
                    ),
                    SizedBox(height: 10,),
                  ],
                ),
              ),
              SizedBox(height: responsive.hp(3)),
              ButtonTheme(
                height: responsive.hp(6.3),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () {
                    if (_formKey.currentState
                        .validate()) {
                      if(foto != null){
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Cargando ...'),
                          duration: Duration(seconds: 3),
                          backgroundColor: Color(0XFF707070),
                        ));
                        _updateImage();
                      }else{
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Agregar imagen'),
                          duration: Duration(seconds: 3),
                        ));
                      }
                    }
                  },
                  textColor: Colors.white,
                  color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: Text(
                      "GUARDAR",
                      style: TextStyle(fontSize: responsive.dp(1.8)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  _SelectImage() async{
    String estado = "";
    foto = await ImagePicker.pickImage(source: ImageSource.gallery);

    if(foto != null ){
      setState(() {
        statusimg = 1;
      });
    }
  }

  _updateImage() async{
    String estado = "";
    estado = await objApiServices.updatePhotoUser(foto);
    if(estado==""){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Imagen no cargada'),
        duration: Duration(seconds: 3),
      ));
    }else{
      Register(estado);
    }
  }

  Register(String img) async {
    int status = 0;
    status = await objApiServices.registerCompany(
      tituloText.text,
      descripcionText.text,
      direccionText.text,
      telefonoText.text,
      emailText.text,
      passwordText.text,
      img,
      currentUser.value.id.toString(),
      tipo,
      categoria
    );
    if (status == 1) {
      Navigator.of(context).pushReplacement(PageRouteBuilder(
          pageBuilder: (BuildContext context, _, __) {
            return RegisterCompanyWidget();
          }, transitionsBuilder:
          (_, Animation<double> animation, __, Widget child) {
        return FadeTransition(opacity: animation, child: child);
      }));
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return messageAlert(tipo: "empresa",);
        },useRootNavigator: false,
        barrierDismissible: false,
      );
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('No Registrado'),
        duration: Duration(seconds: 3),
      ));
    }
  }
}