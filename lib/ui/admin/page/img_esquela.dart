import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/add_img_esquela.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ImgEsquelaWidget extends StatefulWidget {

  @override
  _ImgEsquelaWidgetState createState() => new _ImgEsquelaWidgetState();
}

class _ImgEsquelaWidgetState extends State<ImgEsquelaWidget> {
  ApiServices objApiService = ApiServices();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Text("Fondos de esquelas"),
      ),
      body: esquelas(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (BuildContext context, _, __) {
                return AddImgEsquelaWidget();
              }, transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return FadeTransition(opacity: animation, child: child);
          }));
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.black,
      ),
    );
  }

  Widget esquelas(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return Container(
      height: responsive.heigth,
      child: FutureBuilder(
        future: objApiService.getListImgEsquela(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if (!snapshot.hasData) {
            return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ));
          }else if (snapshot.data.length == 0) {
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 14,
                            ),
                            Text("No hay datos")
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            );
          } else{
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index){
                  var link = snapshot.data[index]["link"];
                  String id = snapshot.data[index]["id"].toString();
                  String titulo = snapshot.data[index]["titulo"];
                  return Card(
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.white70, width: 1),
                        borderRadius: BorderRadius.circular(40),
                      ),
                      margin: EdgeInsets.all(10.0),
                      elevation: 0,
                      child: Container(
                        height: responsive.hp(28),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(
                            color: Colors
                                .black, //                   <--- border color
                            width: 1.5,
                          ),
                          image: DecorationImage(
                            image: CachedNetworkImageProvider(link),
                            fit: BoxFit.fill,
                            colorFilter: ColorFilter.mode(
                                Colors.grey.withOpacity(1),
                                BlendMode.softLight
                            ),
                          ),
                        ),
                        child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.black
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: Column(
                                        children: <Widget>[
                                          Text(titulo, style: TextStyle(fontSize: responsive.dp(2.0), color: Colors.white, fontWeight: FontWeight.bold), maxLines: 1,)
                                        ],
                                      ),
                                    )
                                )
                              ],
                            )
                        )
                      )
                  );
                }
            );
          }
        },
      ),
    );
  }
}