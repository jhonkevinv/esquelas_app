import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/product.dart';
import 'package:esquelas_app/ui/admin/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class CompanyWidget extends StatefulWidget {

  String id;
  CompanyWidget({Key key, this.id}) : super(key: key);
  @override
  _CompanyWidgetState createState() => _CompanyWidgetState();
}

class _CompanyWidgetState extends State<CompanyWidget> {
  ApiServices objApiService = ApiServices();
  bool switchValue = true;

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("EMPRESAS"),
        backgroundColor: Colors.black,
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Container(
        height: responsive.heigth,
        child: FutureBuilder(
          future: objApiService.getListCompany(widget.id),
          builder: (BuildContext context, AsyncSnapshot snapshot){
            if (!snapshot.hasData) {
              return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ));
            }else if (snapshot.data.length == 0) {
              return Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 14,
                              ),
                              Text("No hay datos")
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            }else{
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index){
                  String id = snapshot.data[index]["id"].toString();
                  String titulo = snapshot.data[index]["titulo"].toString();
                  String logo = snapshot.data[index]["logo"].toString();
                  String estado = snapshot.data[index]["estado"].toString();
                  return Padding(
                    padding: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ProductWidget(id: id),));
                              },
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Container(
                                      width: responsive.wp(38),
                                      height: responsive.wp(38),
                                      decoration: BoxDecoration(
                                          borderRadius: new BorderRadius.circular(30.0),
                                          border: Border.all(
                                            color: Colors.black,
                                            width: 1.0,
                                          ),
                                          image: DecorationImage(
                                              image: CachedNetworkImageProvider(logo),
                                              fit: BoxFit.fill
                                          )
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                        ),
                        Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 10, left: 10),
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 10,),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(titulo, style: TextStyle(fontSize: responsive.dp(1.8), fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  CupertinoSwitchNotification(id: id, statusNotification: estado, tipo: "empresa",),
                                ],
                              ),
                            )
                        )
                      ],
                    ),
                  );
                },
              );
            }
          },
        ),
      )
    );
  }
}
