import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/register_admin.dart';
import 'package:esquelas_app/ui/admin/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class AdminListWidget extends StatefulWidget {
  @override
  _AdminListWidgetState createState() => _AdminListWidgetState();
}

class _AdminListWidgetState extends State<AdminListWidget> {

  ApiServices objApiService = ApiServices();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  //bool switchValue = true;

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Administradores"),
        backgroundColor: Colors.black,
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Container(
        height: responsive.heigth,
        child: FutureBuilder(
          future: objApiService.getListUser(),
          builder: (BuildContext context, AsyncSnapshot snapshot){
            if (!snapshot.hasData) {
              return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ));
            }else if (snapshot.data.length == 0) {
              return Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 14,
                              ),
                              Text("No hay datos")
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            }else{
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index){
                  String id = snapshot.data[index]["id"].toString();
                  String tipo_id = snapshot.data[index]["tipos_id"].toString();
                  String nombre = snapshot.data[index]["nombre"].toString();
                  String apellido = snapshot.data[index]["apellido"].toString();
                  String img = snapshot.data[index]["img"].toString();
                  String estado = snapshot.data[index]["estado"].toString();
                  if((tipo_id == "2") && (id != currentUser.value.id.toString())){
                    return Padding(
                      padding: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Container(
                                      width: responsive.wp(35),
                                      height: responsive.wp(35),
                                      decoration: BoxDecoration(
                                          borderRadius: new BorderRadius.circular(30.0),
                                          border: Border.all(
                                            color: Colors.black,
                                            width: 1.0,
                                          ),
                                          image: DecorationImage(
                                              image: CachedNetworkImageProvider(img),
                                              fit: BoxFit.fill
                                          )
                                      ),
                                    ),
                                  )
                                ],
                              )
                          ),
                          Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 10, left: 10),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(nombre, style: TextStyle(fontSize: responsive.dp(1.8), fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(apellido, style: TextStyle(fontSize: responsive.dp(1.8), fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    CupertinoSwitchNotification(id: id, statusNotification: estado, tipo: "administrador",)
                                  ],
                                ),
                              )
                          )
                        ],
                      ),
                    );
                  }else{
                    return Container();
                  }
                },
              );
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (BuildContext context, _, __) {
                return RegisterAdminWidget();
              }, transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return FadeTransition(opacity: animation, child: child);
          }));
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.black,
      ),
    );
  }
}
