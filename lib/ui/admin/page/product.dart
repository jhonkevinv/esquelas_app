import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/widget.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class ProductWidget extends StatefulWidget {
  String id;
  ProductWidget({Key key, this.id}) : super(key: key);

  @override
  _ProductWidgetState createState() => _ProductWidgetState();
}

class _ProductWidgetState extends State<ProductWidget> {

  bool switchValue = true;
  ApiServices objApiService = ApiServices();

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("PRODUCTOS"),
        backgroundColor: Colors.black,
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Container(
        height: responsive.heigth,
        child: FutureBuilder(
          future: objApiService.getListProduct(widget.id),
          builder: (BuildContext context, AsyncSnapshot snapshot){
            if (!snapshot.hasData) {
              return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ));
            }else if (snapshot.data.length == 0) {
              return Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 14,
                              ),
                              Text("No hay datos")
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            }else{
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index){
                  String id = snapshot.data[index]["id"].toString();
                  String titulo = snapshot.data[index]["titulo"].toString();
                  String img = snapshot.data[index]["img"].toString();
                  String precio = snapshot.data[index]["precio"].toString();
                  String estado = snapshot.data[index]["estado"].toString();
                  return Padding(
                    padding: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: (){},
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Container(
                                      width: responsive.wp(38),
                                      height: responsive.wp(38),
                                      decoration: BoxDecoration(
                                          borderRadius: new BorderRadius.circular(30.0),
                                          border: Border.all(
                                            color: Colors.black,
                                            width: 1.0,
                                          ),
                                          image: DecorationImage(
                                              image: CachedNetworkImageProvider(img),
                                              fit: BoxFit.fill
                                          )
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                        ),
                        Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 5, left: 5),
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 10,),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text(titulo+":", style: TextStyle(fontSize: responsive.dp(1.6), fontWeight: FontWeight.bold),textAlign: TextAlign.start,),
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.only(left: 5),
                                            child: Text("Q"+precio, style: TextStyle(fontSize: responsive.dp(1.6)),textAlign: TextAlign.end,),
                                          )
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  CupertinoSwitchNotification(id: id, statusNotification: estado, tipo: "producto",),
                                ],
                              ),
                            )
                        )
                      ],
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}
