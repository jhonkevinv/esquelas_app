import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/admin/page/search.dart';
import 'package:esquelas_app/ui/admin/page/validate_esquela.dart';
import 'package:esquelas_app/ui/admin/tab/page_admin.dart';
import 'package:esquelas_app/ui/admin/tab/search_date.dart';
import 'package:esquelas_app/ui/admin/widget.dart';
import 'package:esquelas_app/ui/user/tab/category.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EsquelaWidget extends StatefulWidget {
  var img;
  String id,
      nombre,
      apellido,
      titulo,
      descripcion,
      fecha_ini,
      fecha_fin,
      hora_ini,
      hora_fin,
      fecha_ini_entierro,
      hora_ini_entierro,
      hora_fin_entierro,
      funeraria_empresas_id,
      cementerio_empresa_id,
      estado,
      page
  ;
  EsquelaWidget(
      {Key key,
        this.id,
        this.img,
        this.nombre,
        this.apellido,
        this.titulo,
        this.descripcion,
        this.fecha_ini,
        this.fecha_fin,
        this.hora_ini,
        this.hora_fin,
        this.fecha_ini_entierro,
        this.hora_ini_entierro,
        this.hora_fin_entierro,
        this.cementerio_empresa_id,
        this.funeraria_empresas_id,
        this.estado,
        this.page
      })
      : super(key: key);

  @override
  _EsquelaWidgetState createState() => new _EsquelaWidgetState();
}

class _EsquelaWidgetState extends State<EsquelaWidget> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ApiServices objApiService = ApiServices();
  //bool switchValue = true;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        if(widget.page == "home"){
          return Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (BuildContext context, _, __) {
                return PageAdminWidget(page: 0,);
              }, transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return FadeTransition(opacity: animation, child: child);
          }));
        }else if(widget.page == "search"){
          return Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (BuildContext context, _, __) {
                return SearchWidget();
              }, transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return FadeTransition(opacity: animation, child: child);
          }));
        }else if(widget.page == "validate"){
          return Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (BuildContext context, _, __) {
                return ValidateEsquelaWidget();
              }, transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return FadeTransition(opacity: animation, child: child);
          }));
        }else{
          return Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (BuildContext context, _, __) {
                return PageAdminWidget(page: 1,);
              }, transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return FadeTransition(opacity: animation, child: child);
          }));
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              if(widget.page == "home"){
                return Navigator.of(context).pushReplacement(PageRouteBuilder(
                    pageBuilder: (BuildContext context, _, __) {
                      return PageAdminWidget(page: 0,);
                    }, transitionsBuilder:
                    (_, Animation<double> animation, __, Widget child) {
                  return FadeTransition(opacity: animation, child: child);
                }));
              }else if(widget.page == "search"){
                return Navigator.of(context).pushReplacement(PageRouteBuilder(
                    pageBuilder: (BuildContext context, _, __) {
                      return SearchWidget();
                    }, transitionsBuilder:
                    (_, Animation<double> animation, __, Widget child) {
                  return FadeTransition(opacity: animation, child: child);
                }));
              }else if(widget.page == "validate"){
                return Navigator.of(context).pushReplacement(PageRouteBuilder(
                    pageBuilder: (BuildContext context, _, __) {
                      return ValidateEsquelaWidget();
                    }, transitionsBuilder:
                    (_, Animation<double> animation, __, Widget child) {
                  return FadeTransition(opacity: animation, child: child);
                }));
              }else{
                return Navigator.of(context).pushReplacement(PageRouteBuilder(
                    pageBuilder: (BuildContext context, _, __) {
                      return PageAdminWidget(page: 1,);
                    }, transitionsBuilder:
                    (_, Animation<double> animation, __, Widget child) {
                  return FadeTransition(opacity: animation, child: child);
                }));
              }
            },
            icon: Icon(
              Icons.arrow_back,
            ),
          ),
          centerTitle: true,
          title: Text("ESQUELA"),
        ),
        body: editProfile(context),
      ),
    );
  }

  Widget editProfile(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        Container(
          height: responsive.hp(29),
          width: responsive.width,
          decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(90), bottomRight: Radius.circular(90)),
              border: Border.all(
                color: Colors
                    .black, //                   <--- border color
                width: 1.5,
              ),
              image: DecorationImage(
                  image: NetworkImage(widget.img),
                  fit: BoxFit.cover
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 20),
          child: Column(
            children: <Widget>[
              Text(widget.nombre,
                style: TextStyle(
                    fontSize: responsive.dp(2.2), fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              Text(widget.apellido,
                style: TextStyle(
                    fontSize: responsive.dp(2.2), fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: responsive.hp(0.5),),
              CupertinoSwitchNotification(id: widget.id, statusNotification: widget.estado, tipo: "esquela", page: widget.page),
              SizedBox(height: responsive.hp(0.5),),
              Text(widget.titulo, style: TextStyle(fontSize: responsive.dp(2), fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
              SizedBox(height: responsive.hp(1),),
              Container(
                height: responsive.hp(12),
                width: responsive.wp(70),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black, //                   <--- border color
                    width: 1.0,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: ListView(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(widget.descripcion),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: responsive.hp(1),),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Text("VELORIO", style: TextStyle(fontSize: responsive.dp(2.2), fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                        SizedBox(height: responsive.hp(0.5),),
                        Text("Fecha", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                        SizedBox(height: responsive.hp(0.5),),
                        Container(
                          height: responsive.hp(4),
                          width: responsive.wp(30),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            border: Border.all(
                              color: Colors.black, //                   <--- border color
                              width: 1.0,
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(widget.fecha_ini, style: TextStyle(fontSize: responsive.dp(1.7)), maxLines: 1,),
                            ],
                          )
                        ),
                        SizedBox(height: responsive.hp(0.5),),
                        Text("Hora", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                        SizedBox(height: responsive.hp(0.5),),
                        Container(
                            height: responsive.hp(4),
                            width: responsive.wp(30),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              border: Border.all(
                                color: Colors.black, //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(widget.hora_ini, style: TextStyle(fontSize: responsive.dp(1.7)),),
                              ],
                            )
                        ),
                        SizedBox(height: responsive.hp(0.5),),
                        Text("Funeraria", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                        SizedBox(height: responsive.hp(0.5),),
                        Container(
                          height: responsive.hp(4),
                          width: responsive.wp(40),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            border: Border.all(
                              color: Colors
                                  .black, //                   <--- border color
                              width: 1.0,
                            ),
                          ),
                          child: FutureBuilder(
                            future: objApiService.getListCompanyTotal("2"),
                            builder: (BuildContext context, AsyncSnapshot snapshot){
                              if (!snapshot.hasData) {
                                return Container(
                                    child: Center(
                                      child: CircularProgressIndicator(),
                                    ));
                              }else if (snapshot.data.length == 0) {
                                return Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Flexible(
                                            child: Column(
                                              children: <Widget>[
                                                SizedBox(
                                                  height: 14,
                                                ),
                                                Text("No hay datos")
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                              } else{
                                return ListView.builder(
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (context, index){
                                      String id = snapshot.data[index]["id"].toString();
                                      String direccion = snapshot.data[index]["direccion"].toString();
                                      if(widget.funeraria_empresas_id == id){
                                        return Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            SizedBox(height: responsive.hp(0.8),),
                                            Text(direccion)
                                          ],
                                        );
                                      }else{
                                        return Text("");
                                      }
                                    }
                                );
                              }
                            },
                          ),),
                      ],
                    )
                  ),
                  Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Text("ENTIERRO", style: TextStyle(fontSize: responsive.dp(2.2), fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Text("Fecha", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Container(
                              height: responsive.hp(4),
                              width: responsive.wp(30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.0),
                                border: Border.all(
                                  color: Colors.black, //                   <--- border color
                                  width: 1.0,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(widget.fecha_ini_entierro, style: TextStyle(fontSize: responsive.dp(1.7)), maxLines: 1,),
                                ],
                              )
                          ),
                          SizedBox(height: responsive.hp(0.5),),
                          Text("Hora", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Container(
                              height: responsive.hp(4),
                              width: responsive.wp(30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.0),
                                border: Border.all(
                                  color: Colors.black, //                   <--- border color
                                  width: 1.0,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(widget.hora_ini_entierro, style: TextStyle(fontSize: responsive.dp(1.7)),),
                                ],
                              )
                          ),
                          SizedBox(height: responsive.hp(0.5),),
                          Text("Cementerio", style: TextStyle(fontSize: responsive.dp(1.5)), textAlign: TextAlign.center,),
                          SizedBox(height: responsive.hp(0.5),),
                          Container(
                            height: responsive.hp(4),
                            width: responsive.wp(40),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              border: Border.all(
                                color: Colors
                                    .black, //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: FutureBuilder(
                              future: objApiService.getListCompanyTotal("3"),
                              builder: (BuildContext context, AsyncSnapshot snapshot){
                                if (!snapshot.hasData) {
                                  return Container(
                                      child: Center(
                                        child: CircularProgressIndicator(),
                                      ));
                                }else if (snapshot.data.length == 0) {
                                  return Container(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Flexible(
                                              child: Column(
                                                children: <Widget>[
                                                  SizedBox(
                                                    height: 14,
                                                  ),
                                                  Text("No hay datos")
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                } else{
                                  return ListView.builder(
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (context, index){
                                        String id = snapshot.data[index]["id"].toString();
                                        String direccion = snapshot.data[index]["direccion"].toString();
                                        if(widget.cementerio_empresa_id == id){
                                          return Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              SizedBox(height: responsive.hp(0.8),),
                                              Text(direccion)
                                            ],
                                          );
                                        }else{
                                          return Text("");
                                        }
                                      }
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      )
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }
}