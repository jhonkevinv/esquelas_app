import 'dart:io';

import 'package:esquelas_app/model/user_model.dart';
import 'package:esquelas_app/services/api_services.dart';
import 'package:esquelas_app/ui/user/tab/page_user.dart';
import 'package:esquelas_app/util/colors.dart';
import 'package:esquelas_app/util/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class RegisterUserWidget extends StatefulWidget {

  @override
  _RegisterUserWidgetState createState() => new _RegisterUserWidgetState();
}

class _RegisterUserWidgetState extends State<RegisterUserWidget> {

  bool _passwordVisible = false;
  ApiServices objApiServices = ApiServices();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  UserModel _user = UserModel();
  File foto;
  int statusimg = 0;
  String init_foto = "https://p.kindpng.com/picc/s/21-211168_transparent-person-icon-png-png-download.png";

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: editProfile(context),
    );
  }

  Widget editProfile(BuildContext context){
    final Responsive responsive = Responsive.of(context);
    return ListView(
      children: <Widget>[
        Container(
          height: responsive.hp(31),
          width: responsive.width,
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(90), bottomRight: Radius.circular(90)),
              border: Border.all(
                color: Colors.black, //                   <--- border color
                width: 1.5,
              ),
              image: DecorationImage(
                  image: statusimg == 0 ?NetworkImage(init_foto): FileImage(foto),
                  fit: BoxFit.cover
              )
          ),
          child: Stack(
            children: <Widget>[
              Positioned(
                  bottom: 8,
                  right: 12,
                  child: GestureDetector(
                      onTap: _SelectImage,
                      child: Container(
                        height: responsive.wp(10),
                        width: responsive.wp(10),
                        child: Icon(
                          Icons.edit,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.black,
                            shape: BoxShape.circle
                        ),
                      )))
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 30, left: 30, bottom: 20),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text("REGISTRO", textAlign: TextAlign.center, style: TextStyle(fontSize: responsive.dp(3)),),
                  )
                ],
              ),
              SizedBox(height: responsive.hp(2),),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            onChanged: (input) => _user.nombre = input,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Nombre" : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.person, color: Colors.black,),
                              labelText: "Nombre",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: primaryColorFont,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            onChanged: (input) => _user.apellido = input,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Apellido" : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.person, color: Colors.black,),
                              labelText: "Apellido",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: primaryColorFont,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            onChanged: (input) => _user.email = input,
                            keyboardType: TextInputType.emailAddress,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar correo" : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.email, color: Colors.black,),
                              labelText: "Correo",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: primaryColorFont,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            onChanged: (input) => _user.pass = input,
                            obscureText: !_passwordVisible,
                            validator: (input) => input.length == 0
                                ? "Ingresar contraseña"
                                : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.enhanced_encryption, color: Colors.black,),
                              labelText: "Contraseña",
                              labelStyle: TextStyle(color: Colors.black),
                              fillColor: Colors.black,
                              border: InputBorder.none,
                              suffixIcon: IconButton(
                                icon: _passwordVisible
                                    ? Icon(
                                  Icons.visibility_off,
                                  color: Colors.black45,
                                )
                                    : Icon(
                                  Icons.visibility,
                                  color: Colors.black45,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _passwordVisible = !_passwordVisible;
                                  });
                                },
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: primaryColorFont,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            onChanged: (input) => _user.tel = input,
                            keyboardType: TextInputType.text,
                            validator: (input) =>
                            input.length == 0 ? "Ingresar Teléfono" : null,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              prefixIcon: Icon(Icons.phone_in_talk, color: Colors.black,),
                              labelText: "Teléfono",
                              labelStyle: TextStyle(color: Colors.black),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: primaryColorFont,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: responsive.hp(3)),
              ButtonTheme(
                minWidth: responsive.wp(100),
                height: responsive.hp(6.3),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () {
                    if (_formKey.currentState
                        .validate()) {
                      if(foto != null){
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Cargando ...'),
                          duration: Duration(seconds: 3),
                          backgroundColor: Color(0XFF707070),
                        ));
                        _updateImage();
                      }else{
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Cargando'),
                          duration: Duration(seconds: 3),
                        ));
                        Register(init_foto);
                      }
                    }
                  },
                  textColor: Colors.white,
                  color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: Text(
                      "REGISTRAR",
                      style: TextStyle(fontSize: responsive.dp(1.8)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  _SelectImage() async{
    foto = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(foto != null ){
      setState(() {
        statusimg = 1;
      });
    }
  }

  _updateImage() async{
    String estado = "";
    estado = await objApiServices.updatePhotoUser(foto);
    if(estado==""){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Imagen no cargada'),
        duration: Duration(seconds: 3),
      ));
    }else{
      Register(estado);
    }
  }

  Register(String img) async {
    int status = 0;
    status = await objApiServices.registerUser(
        _user.nombre,
        _user.apellido,
        _user.email,
        _user.pass,
        _user.tel,
        1,
        img
    );
    if (status == 1) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => PageUserWidget(page: 0,),));
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Usuario no registrado'),
        duration: Duration(seconds: 3),
      ));
    }
  }
}