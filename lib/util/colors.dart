import 'dart:ui';
import 'package:flutter/material.dart';

const primaryColor = const Color(0XFFFFFFFF);
const primaryColorFont = const Color(0XFF1E81F5);
