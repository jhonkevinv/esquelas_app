class UserModel{
  int id;
  String nombre;
  String apellido;
  String email;
  String pass;
  String tel;
  String direccion;
  String img;
  int tipos_id;
  int empresas_cantidad;
  int esquelas_cantidad;
  int pesame_cantidad;
  String estado;

  UserModel({
    this.id,
    this.nombre,
    this.apellido,
    this.email,
    this.pass,
    this.tel,
    this.direccion,
    this.img,
    this.tipos_id,
    this.empresas_cantidad,
    this.esquelas_cantidad,
    this.pesame_cantidad,
    this.estado,
  });

  UserModel.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      nombre = jsonMap['nombre'];
      apellido = jsonMap['apellido'];
      email = jsonMap['email']!= null ? jsonMap['email'] : '';
      pass = jsonMap["pass"]!= null ? jsonMap['pass'] : '';
      tel = jsonMap['tel']!= null ? jsonMap['tel'] : '';
      direccion = jsonMap['direccion']!= null ? jsonMap['direccion'] : '';
      img = jsonMap["img"]!= null ? jsonMap['img'] : '';
      tipos_id = jsonMap["tipos_id"]!= null ? jsonMap['tipos_id'] : 0;
      empresas_cantidad = jsonMap["empresas_cantidad"]!= null ? jsonMap[''] : 0;
      esquelas_cantidad = jsonMap["esquelas_cantidad"]!= null ? jsonMap['email'] : 0;
      pesame_cantidad = jsonMap["pesame_cantidad"]!= null ? jsonMap['email'] : 0;
      estado = jsonMap["estado"] != null ? jsonMap['email'] : '';
    } catch (e) {
      print(e);
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["nombre"] = nombre;
    map["apellido"] = apellido;
    map["email"] = email;
    map["pass"] = pass;
    map["tel"] = tel;
    map["direccion"] = direccion;
    map["img"] = img;
    map["tipos_id"] = tipos_id;
    map["empresas_cantidad"] = empresas_cantidad;
    map["esquelas_cantidad"] = esquelas_cantidad;
    map["pesame_cantidad"] = pesame_cantidad;
    map["estado"] = estado;
    return map;
  }
}